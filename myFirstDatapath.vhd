--Derrick Brandt
--CPRE 381 Lab 2 Section 6
--Professor Berk

library IEEE;
use IEEE.std_logic_1164.all;

--My First Datapath

--entity
entity my_first_datapath is 
port(i_read_rs, i_read_rt, i_write_rd 	: in std_logic_vector(4 downto 0);
     ALUsc	                         : in std_logic;
     neAdd_Sub	                         : in std_logic;
     i_WE                               : in std_logic;
     i_CLK                              : in std_logic;
     i_RST                              : in std_logic;
     i_immediate                        : in std_logic_vector(31 downto 0));
end my_first_datapath;

--architecture
architecture my_first_datapath_arch of my_first_datapath is 
--components

--MUX 2:1 w/ 32 bit bus
component mux2t1_N is
generic(N : integer := 32);
port(i_s          		: in std_logic;
     i_D0         		: in std_logic_vector(N-1 downto 0);
     i_D1         		: in std_logic_vector(N-1 downto 0);
     o_O          		: out std_logic_vector(N-1 downto 0));
end component;

--adder subtractor
component adder_subtractor is 
generic(N : integer := 32);
port(A, B		     : in std_logic_vector(N-1 downto 0);
     nAdd_Sub		: in std_logic;
     Add_Sub_out	: out std_logic_vector(N-1 downto 0));
end component;

--registerFile
component registerFile is 
generic(N : integer := 32);
port (read_reg_1, read_reg_2	: in std_logic_vector(4 downto 0);
      i_WE			: in std_logic;
      CLK			     : in std_logic;
      RST			     : in std_logic;
      i_reg_data		: in std_logic_vector(31 downto 0);
      i_writeto_reg		: in std_logic_vector(4 downto 0);
      o_read_data_1, o_read_data_2 : out std_logic_vector(31 downto 0));
end component;
--signals
signal s_b_mux_out, s_addersubber_out	: std_logic_vector(31 downto 0);
signal s_o_read_data_1, s_o_read_data_2 : std_logic_vector(31 downto 0);
--signal for reg file out


--begin
begin
	regiFile: registerFile port map(read_reg_1 => i_read_rs,
                                     read_reg_2 => i_read_rt,
                                     i_WE       => i_WE,
                                     CLK        => i_CLK,
                                     RST        => i_RST,
                                     i_reg_data => s_addersubber_out,
                                     i_writeto_reg => i_write_rd,
                                     o_read_data_1 => s_o_read_data_1,
                                     o_read_data_2 => s_o_read_data_2);
	
	addiSubi: adder_subtractor port map(A => s_o_read_data_1,
                                         B => s_b_mux_out,
                                         nAdd_Sub => neAdd_Sub,
                                         Add_Sub_out => s_addersubber_out);

	muxi:	  mux2t1_N port map(i_s => ALUsc,
                                   i_D0 => s_o_read_data_2,
                                   i_D1 => i_immediate,
                                   o_O => s_b_mux_out);

end my_first_datapath_arch;