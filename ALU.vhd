--TEAM G-NULL
--CPRE 381 PROJECT 1 LAB SECTION 6
--Professor Berk

--ALU entity and architecture

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--entity
entity ALU is 
generic(N : integer := 32);
-- #TODO Add ALU control input to the ALU entity
port(i_A           : in std_logic_vector(N-1 downto 0);
     i_B           : in std_logic_vector(N-1 downto 0);
     i_ALU_control : in std_logic_vector(3 downto 0);
     result_out    : out std_logic_vector(N-1 downto 0);
     zero_out      : out std_logic;
     o_overflow    : out std_logic);
end ALU;

--architecture
architecture ALU_arch of ALU is 
--components; #TODO; adder and what else?

--adder subtractor
component adder_subtractor is 
generic(N : integer := 32);
     port(A, B		: in std_logic_vector(N-1 downto 0);
          nAdd_Sub		: in std_logic;
          overflow		: out std_logic;
          Add_Sub_out	: out std_logic_vector(N-1 downto 0));
end component;

-- --BITWISE AND
component and32 is
     port(i_A          : in std_logic_vector(31 downto 0);
          i_B          : in std_logic_vector(31 downto 0);
          o_O          : out std_logic_vector(31 downto 0));
end component;
-- -- BITWISE OR
component or32 is
     port(i_A          : in std_logic_vector(31 downto 0);
          i_B          : in std_logic_vector(31 downto 0);
          o_O          : out std_logic_vector(31 downto 0));
end component;
-- --BITWISE XOR
component xor32 is
     port(i_A          : in std_logic_vector(31 downto 0);
          i_B          : in std_logic_vector(31 downto 0);
          o_O          : out std_logic_vector(31 downto 0));
end component;
-- --BITWISE NOR
component nor32 is
     port(i_A          : in std_logic_vector(31 downto 0);
          i_B          : in std_logic_vector(31 downto 0);
          o_O          : out std_logic_vector(31 downto 0));
end component;

component branchLogic is 
--generic
port (i_select          : in std_logic;
      add_sub_result    : in std_logic_vector(31 downto 0);
      zero_out          : out std_logic);
end component;
   
component lui is
     port(i_A          : in std_logic_vector(31 downto 0);
          o_O          : out std_logic_vector(31 downto 0));
end component;

component sltiLogic is 
     --generic
     port (i_A                      : in std_logic_vector(31 downto 0);
          i_B                       : in std_logic_vector(31 downto 0);
          one_or_zero_out           : out std_logic_vector(31 downto 0));
end component;

--Barrel Shifter
component shiftlogic is
     port(i_A            : in std_logic_vector(31 downto 0);
          i_S            : in std_logic_vector(4 downto 0);
          i_AorL         : in std_logic;
          i_RorL         : in std_logic;
          o_F            : out std_logic_vector(31 downto 0));
end component;

--intermediary signals; #TODO;
signal s_nadd_sub , s_branch_zero_out                   : std_logic;
signal s_select                                         : std_logic := '0';
signal s_result_Add_Sub_out                             : std_logic_vector(31 downto 0);
signal s_result_and_out, s_result_or_out, s_result_xor_out, s_result_nor_out, s_result_lui_out, s_result_slti_out, s_result_sll_out, s_result_srl_out, s_result_sra_out : std_logic_vector(31 downto 0);

--begin
begin

     

     addersubber: adder_subtractor port map(A           => i_A,
                                            B           => i_B,
                                            nAdd_Sub    => s_nadd_sub,
                                            overflow    => o_overflow,
                                            Add_Sub_out => s_result_Add_Sub_out);
     -- hard code to zero rn #TODO
     --beq perform subtraction: if equal then zero_out = 1; bne perform subtraction: if equal then zero_out = 0;.
     branch_logic_alu: branchLogic port map(i_select       => s_select,
                                            add_sub_result => s_result_Add_Sub_out,
                                            zero_out       => s_branch_zero_out);

     ALU_and: and32 port map (i_A        => i_A,
                              i_B        => i_B,
                              o_O        => s_result_and_out);
     
     ALU_or: or32 port map ( i_A        => i_A,
                              i_B        => i_B,
                              o_O        => s_result_or_out);
                              
     ALU_xor: xor32 port map (i_A        => i_A,
                              i_B        => i_B,
                              o_O        => s_result_xor_out);
                              
     ALU_nor: nor32 port map (i_A        => i_A,
                              i_B        => i_B,
                              o_O        => s_result_nor_out);

     ALU_lui: lui port map (i_A => i_B,
                            o_O => s_result_lui_out);

     ALU_slti: sltiLogic port map(i_A             => i_A,
                                  i_B             => i_B,
                                  one_or_zero_out => s_result_slti_out);

     -- ALU_shift: shiftlogic port map (i_A            => i_A,
     --                                 i_S            => i_S,
     --                                 i_AorL         => i_AorL,
     --                                 i_RorL         => i_RorL,
     --                                 o_F            => result_out);
     

     
     --add or subtract selector
     with (i_ALU_control) select
          s_nadd_sub <= '0' when "0001",
                        '1' when "0111",
                        '0' when others;

     --decision maker for the select signal in branch logic;                
     with (i_ALU_control) select
     s_select <= '1' when "1100", --select one(1) when bne active. when is bne active??
                 '0' when others;

     --output selector
     with (i_ALU_control) select
          result_out <= s_result_Add_Sub_out when "0001",
                        s_result_Add_Sub_out when "0111",
                        s_result_and_out when     "0010",
                        s_result_or_out when      "0011",       --#TODO
                        s_result_xor_out when     "0100",
                        s_result_nor_out when     "0101",
                        s_result_lui_out when     "1101",
                        s_result_slti_out when    "1110",

                          x"00000000" when others;

     --zero output selector; for branching, if the output from the subtraction operation is zero, then A=B & zero_out should be set high.
     zero_out <= s_branch_zero_out;
          
end ALU_arch;