--TEAM G-NULL
--CPRE 381 PROJECT 1 LAB SECTION 6
--Professor Berk

--ALU entity and architecture

--#README-- PLEASE COMPILE THIS FILE USING VHDL 2008.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--entity
entity shiftLeft2 is 
    generic(N   : integer := 26);
    port(i_A    : in std_logic_vector(N-1 downto 0);
         o_F    : out std_logic_vector(N-1 downto 0));
end shiftLeft2;

--architecture
architecture shiftLeft2_arch of shiftLeft2 is 
--components

--signals

--begin
begin 
    o_F <= i_A sll 2;
end shiftLeft2_arch;