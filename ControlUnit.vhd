--TEAM G-NULL
--CPRE 381 PROJECT 1 LAB SECTION 6
--Professor Berk

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--Control Unit entity and architecture

--entity

--Breakdown: Must intake 6 bits and determine what outputs must be; i_instruction_bits will get assigned the s_Inst signal in MIPS_Processor.vhd.
entity CONTROLUNIT is 
    generic(N : integer := 32);
    port(i_instruction_bits_high : in std_logic_vector(5 downto 0);
         i_instruction_bits_low  : in std_logic_vector(5 downto 0);
         o_reg_dst               : out std_logic;
         o_jump                  : out std_logic; --#TODO
         o_branch                : out std_logic; --#TODO
         o_mem_to_reg            : out std_logic;
         o_sign_extend_select    : out std_logic;
         o_mem_write             : out std_logic;
         o_ALUsrc                : out std_logic;
         o_reg_write             : out std_logic;
         o_halt_signal           : out std_logic;
         o_jal                   : out std_logic;
         o_choose_hard_one       : out std_logic;
         o_choose_PC_plus_eight  : out std_logic;
         o_jr                    : out std_logic; --signal that chooses to load r[31] into program counter;
         o_ALU_Controller        : out std_logic_vector(3 downto 0)); --this is the Opcode/funct code
end CONTROLUNIT;

--architecture
architecture CONTROLUNIT_ARCH of CONTROLUNIT is
--components
component instructionDecoder is
    generic(N : integer := 32);
    port(opcode                         : in std_logic_vector(5 downto 0);
         registerDestination            : out std_logic;
         jumpToInstructionAddress       : out std_logic;
         branchToInstructionAddress     : out std_logic;
         memoryToRegisterMux            : out std_logic;
         memoryWriteEnable              : out std_logic;
         registerWriteEnable            : out std_logic;
         ALU_sourceRegister             : out std_logic;
         ALU_operation                  : out std_logic_vector(3 downto 0);
         signExtendSelect               : out std_logic;
         haltSignalOut                  : out std_logic
         );
end component;

component ALU_Control is 
port(i_instruction_bits      : in std_logic_vector(5 downto 0); --i_instruction_bits = OPcode on MIPS green sheet.
     i_ALU_Op                : in std_logic_vector(3 downto 0);
     o_ALU_Control           : out std_logic_vector(3 downto 0));
end component;

component jumpControlLogic is
     generic(N : integer := 32);
     port(opcode                           : in std_logic_vector(5 downto 0); --selects whether the write register takes the rd or rt value.
         o_jal                            : out std_logic;
         o_choose_hard_one                : out std_logic;
         o_choose_PC_plus_eight           : out std_logic;
         o_jr                             : out std_logic;
         i_enable                         : in std_logic
         );
end component;

--signals
signal s_ALU_Op             : std_logic_vector(3 downto 0);
signal s_ALU_Controller_out : std_logic_vector(3 downto 0);
signal s_jump               : std_logic;
signal jump_logic_helper    : std_logic_vector(5 downto 0);
signal s_enable             : std_logic;
--begin
    begin
    I_Type: instructionDecoder port map(opcode                         => i_instruction_bits_high,
                                        registerDestination            => o_reg_dst,
                                        jumpToInstructionAddress       => s_jump,
                                        branchToInstructionAddress     => o_branch,
                                        memoryToRegisterMux            => o_mem_to_reg,
                                        memoryWriteEnable              => o_mem_write,
                                        registerWriteEnable            => o_reg_write,
                                        ALU_sourceRegister             => o_ALUsrc,
                                        ALU_operation                  => s_ALU_Op,
                                        signExtendSelect               => o_sign_extend_select,
                                        haltSignalOut                  => o_halt_signal);

    R_Type: ALU_Control port map(i_instruction_bits => i_instruction_bits_low,
                                 i_ALU_Op           => s_ALU_Op,
                                 o_ALU_Control      => s_ALU_Controller_out);

    J_Type: jumpControlLogic port map (opcode                    => jump_logic_helper,
                                        o_jal                    => o_jal,
                                        o_choose_hard_one        => o_choose_hard_one,
                                        o_choose_PC_plus_eight   => o_choose_PC_plus_eight,
                                        o_jr                     => o_jr,
                                        i_enable                 => s_enable);

     with (i_instruction_bits_high) select
     o_jump <= '1' when "000011",
               s_jump when others;
     
               -- Jump, Jump and Link, and Jump return 
     j_control: process (s_enable,i_instruction_bits_high, i_instruction_bits_low)
     begin
          if     (i_instruction_bits_low = "001000" and i_instruction_bits_high = "000000")  then s_enable <= '1';
          elsif (i_instruction_bits_high = "000011") then s_enable <= '1';
          end if;
     end process j_control;

     with (i_instruction_bits_low) select
     jump_logic_helper <= "001000" when "001000",
                         i_instruction_bits_high when others;


    with (i_instruction_bits_high) select
    o_ALU_Controller <= s_ALU_Controller_out when "000000",
                        s_ALU_Op when others;                
    
end CONTROLUNIT_ARCH;