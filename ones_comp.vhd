--Derrick Brandt
--CPRE 381 Lab 1 Section 6
--Professor Berk

library IEEE;
use IEEE.std_logic_1164.all;

--entity
entity ones_comp is 
	generic(N : integer:= 32);
	port(i_IN		: in std_logic_vector(N-1 downto 0);
     	     o_OUT		: out std_logic_vector(N-1 downto 0));
end ones_comp;

--architecture
architecture ones_comp_arch of ones_comp is

--component
component invg is
	port(i_A          : in std_logic;
     	     o_F          : out std_logic);
end component;

--begin
	begin
	G_NBITCOMP: for i in 0 to N-1 generate
		COMPI: invg port map (
				i_A => i_IN(i),
				o_F => o_OUT(i));
	end generate;

end ones_comp_arch;