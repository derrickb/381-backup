##Derrick Brandt
##CPRE 381 Lab 2 Section 6
##10/02/21
##Professor Berk

##Test Bench 2nd Datapath File 
vcom -reportprogress 300 -work work {/home/derrickb/derrick_b/CPRE 381/Lab2/Lab2/MUX32Package.vhd} -2008
vcom -reportprogress 300 -work work {/home/derrickb/derrick_b/CPRE 381/Lab2/Lab2/*.vhd} -2008
vcom -reportprogress 300 -work work {/home/derrickb/derrick_b/CPRE 381/Lab2/Lab2/Lab1Files/*.vhd} -2008

#IT WORKS! HA HA

#Test starting simulation
vsim work.tb_my_second_datapath

#Add waves

add wave -position insertpoint sim:/tb_my_second_datapath/DUTSECONDPATH/*

add wave -position insertpoint  \
sim:/tb_my_second_datapath/DUTSECONDPATH/regiFile/s_reg_out

add wave -position insertpoint  \
sim:/tb_my_second_datapath/DUTSECONDPATH/memory/ram


#log command for adding future waves
log -r *

mem load -infile dmem2.hex -format hex /tb_my_second_datapath/DUTSECONDPATH/memory/ram

run 2400 ns