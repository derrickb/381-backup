-------------------------------------------------------------------------
-- Henry Duwe
-- Department of Electrical and Computer Engineering
-- Iowa State University
-------------------------------------------------------------------------


-- MIPS_Processor.vhd
-------------------------------------------------------------------------
-- DESCRIPTION: This file contains a skeleton of a MIPS_Processor  
-- implementation.

-- 01/29/2019 by H3::Design created.
-------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;

entity MIPS_Processor is
  generic(N : integer := 32);
  port(iCLK            : in std_logic;
       iRST            : in std_logic;
       iInstLd         : in std_logic;
       iInstAddr       : in std_logic_vector(N-1 downto 0);
       iInstExt        : in std_logic_vector(N-1 downto 0);
       oALUOut         : out std_logic_vector(N-1 downto 0)); -- TODO: Hook this up to the output of the ALU. It is important for synthesis that you have this output that can effectively be impacted by all other components so they are not optimized away.

end  MIPS_Processor;


architecture structure of MIPS_Processor is

  -- TODO: You may add any additional signals or components your implementation 
  --       requires below this comment
--------------------------------------------------------------------------------------------------------------------------
 ------------------COMPONENTS-------------------------------
 
--memory component for instructions and data
  component mem is
    generic(ADDR_WIDTH : integer;
            DATA_WIDTH : integer);
    port(
          clk          : in std_logic;
          addr         : in std_logic_vector((ADDR_WIDTH-1) downto 0);
          data         : in std_logic_vector((DATA_WIDTH-1) downto 0);
          we           : in std_logic := '1';
          q            : out std_logic_vector((DATA_WIDTH -1) downto 0));
    end component;
  
  -- #TODO BUILD OUT CONTROL UNIT
  component CONTROLUNIT is 
  generic(N : integer := 32);
  port(i_instruction_bits_high : in std_logic_vector(5 downto 0);
       i_instruction_bits_low  : in std_logic_vector(5 downto 0);
       o_reg_dst               : out std_logic;
       o_jump                  : out std_logic; --#TODO
       o_branch                : out std_logic; --#TODO
       o_mem_to_reg            : out std_logic;
       o_sign_extend_select    : out std_logic;
       o_mem_write             : out std_logic;
       o_ALUsrc                : out std_logic;
       o_reg_write             : out std_logic;
       o_halt_signal           : out std_logic;
       o_jal                   : out std_logic;
       o_choose_hard_one       : out std_logic;
       o_choose_PC_plus_eight  : out std_logic;
       o_jr                    : out std_logic; --signal that chooses to load r[31] into program counter;
       o_ALU_Controller        : out std_logic_vector(3 downto 0)); --this is the Opcode/funct code
  end component;

    --register file
  component registerFile is 
    generic(N : integer := 32);
    port (read_reg_1, read_reg_2	: in std_logic_vector(4 downto 0);
          i_WE			: in std_logic;
          CLK			     : in std_logic;
          RST			     : in std_logic;
          i_reg_data		: in std_logic_vector(31 downto 0);
          i_writeto_reg		: in std_logic_vector(4 downto 0);
          o_read_data_1, o_read_data_2 : out std_logic_vector(31 downto 0));
  end component;

  --adder subtractor; #TODO TODO TODO --Do we only need adder_subtractor?
  component adder_subtractor is 
    generic(N : integer := 32);
    port(A, B		     : in std_logic_vector(N-1 downto 0);
        nAdd_Sub		: in std_logic;
        Add_Sub_out	: out std_logic_vector(N-1 downto 0));
  end component;

  --#TODO BUILD ALU
  component ALU is 
    generic(N : integer := 32);
    port(i_A           : in std_logic_vector(N-1 downto 0);
        i_B           : in std_logic_vector(N-1 downto 0);
        i_ALU_control : in std_logic_vector(3 downto 0);
        result_out    : out std_logic_vector(N-1 downto 0);
        zero_out      : out std_logic;
        o_overflow      : out std_logic);
  end component;

  --#TODO BUILD ALU_CONTROL
  component ALU_control is 
    port(i_instruction_bits      : in std_logic_vector(5 downto 0);
         i_ALU_Op                : in std_logic_vector(3 downto 0);
         o_ALU_Control           : out std_logic_vector(3 downto 0));
  end component;

  --#TODO BUILD SHIFTER
  component shifter is 
  -- generic(N : integer := 32);
  -- port();
  end component;

  --32 bit 2:1 MUX to be used in multiple places
  component mux2t1_N is
    generic(N : integer := 32); -- Generic of type integer for input/output data width. Default value is 32.
    port(i_S          : in std_logic;
        i_D0         : in std_logic_vector(N-1 downto 0);
        i_D1         : in std_logic_vector(N-1 downto 0);
        o_O          : out std_logic_vector(N-1 downto 0));

  end component;

  --extender
  component bit_width_extender is 
    generic(N : integer := 32);
    port(control	: in std_logic;
        i_A	     : in std_logic_vector(15 downto 0);
        o_F	: out std_logic_vector(N-1 downto 0));
  end component;

  --adder for Program Counter +4
  component n_bit_adder is 
    generic(N : integer := 32); -- Generic of type integer for input/output data width. Default value is 32.
    port (An_in, Bn_in	: in std_logic_vector(N-1 downto 0);
          Cn_in		: in std_logic;
          Cn_out		: out std_logic; 
          Sn_out		: out std_logic_vector(N-1 downto 0));
  end component;

  --Register for Program Counter
 component pcreg is

  port(i_CLK        : in std_logic;     -- Clock input
       i_RST        : in std_logic;     -- Reset input
       i_WE         : in std_logic;     -- Write enable input
       i_D          : in std_logic_vector(31 downto 0);     -- Data value input
       o_Q          : out std_logic_vector(31 downto 0));   -- Data value output

end component;


  -- AND gate component for branching
  component andg2 is 
    port(i_A          : in std_logic;
         i_B          : in std_logic;
         o_F          : out std_logic);
    end component;

    -- jump logic entity 
    component jumpLogic is 
      generic(N   : integer   := 26);
      port(i_instruction_bits : in std_logic_vector(N-1 downto 0);
          i_pc_adder_bits    : in std_logic_vector(N + 5 downto 0);
          o_F                : out std_logic_vector(N + 5 downto 0));
    end component;

    -- branch logic block
    component branchLogicBlock is 
    port(i_pc_adder_instruction : in std_logic_vector(31 downto 0);
         i_sign_extended_num    : in std_logic_vector(31 downto 0);
         i_branch               : in std_logic;
         i_alu_zero             : in std_logic;
         o_alu_result           : out std_logic_vector(31 downto 0);
         o_mux_out_F            : out std_logic_vector(31 downto 0));

    end component;
    


--------------------------------------------------------------------------------------------------------------------------
 ------------------------------------------------------------SIGNALS-------------------------------
 ------------------SIGNALS GIVEN BY BERK-------------------------------
-- Required data memory signals
  signal s_DMemWr       : std_logic; -- TODO: use this signal as the final active high data memory write enable signal
  signal s_DMemAddr     : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the final data memory address input
  signal s_DMemData     : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the final data memory data input
  signal s_DMemOut      : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the data memory output
 
  -- Required register file signals 
  signal s_RegWr        : std_logic; -- TODO: use this signal as the final active high write enable input to the register file
  signal s_RegWrAddr    : std_logic_vector(4 downto 0); -- TODO: use this signal as the final destination register address input
  signal s_RegWrData    : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the final data memory data input

  -- Required instruction memory signals
  signal s_IMemAddr     : std_logic_vector(N-1 downto 0); -- Do not assign this signal, assign to s_NextInstAddr instead
  signal s_NextInstAddr : std_logic_vector(N-1 downto 0); -- TODO: use this signal as your intended final instruction memory address input.
  signal s_Inst         : std_logic_vector(N-1 downto 0); -- TODO: use this signal as the instruction signal 

  -- Required halt signal -- for simulation
  signal s_Halt         : std_logic;  -- TODO: this signal indicates to the simulation that intended program execution has completed. (Opcode: 01 0100)

  -- Required overflow signal -- for overflow exception detection
  signal s_Ovfl         : std_logic;  -- TODO: this signal indicates an overflow exception would have been initiated
 ------------------END SIGNALS GIVEN BY BERK-------------------------------

  
  ------------------INSTRUCTION FETCH SIGNALS-------------------------------

  --Fetch loop signal hard coded to 4 for PC + 4; carry in hardcode to 0 for adder; 
  signal s_literal_four              : std_logic_vector(31 downto 0) := x"00000004";
  signal s_literal_thirty_one        : std_logic_vector(4 downto 0) := "11111";
  signal s_literal_one               : std_logic_vector(31 downto 0) := x"00000000";
  signal s_carry_in_equals_zero      : std_logic := '0';
  signal s_carry_out                 : std_logic;

  signal pc_adder_out           : std_logic_vector(31 downto 0);
  signal s_proper_instruction   : std_logic_vector(31 downto 0);

  signal s_jump_logic_out : std_logic_vector(31 downto 0); 
  ------------------END INSTRUCTION FETCH SIGNALS-------------------------------
 
 ------------------INSTRUCTION DECODE SIGNALS-------------------------------

  --intermediate signals for CONTROLUNIT
  signal s_reg_dst,
         s_jump, 
         s_branch,  
         s_mem_to_reg, 
         s_sign_extend_select, 
         s_ALU_src, 
         s_jal,
         s_jr,
         s_choose_PC_plus_eight,
         s_choose_one : std_logic; 

  signal s_ALU_control_o : std_logic_vector(3 downto 0);
  --intermediate signal for rt/rd MUX
  signal s_mux_write_reg_out  : std_logic_vector(4 downto 0);

   --IMPORTANT SIGNALS FOR REG OUTS
   signal s_read_data_1_o, s_read_data_2_o : std_logic_vector(31 downto 0); --rs=data1 followed by rt=data2

   --Intermediate signal for extender output 
  signal s_extend_out	 : std_logic_vector(31 downto 0);

  --signal for choose hard one mux out
  signal s_jal_cho_out : std_logic_vector(31 downto 0);

 ------------------END INSTRUCTION DECODE SIGNALS-------------------------------


   ------------------EXECUTE SIGNALS-------------------------------
   signal s_alu_mux_out    : std_logic_vector(31 downto 0);
   signal s_alu_zero_out   : std_logic;
   signal s_alu_result_out : std_logic_vector(31 downto 0);
  ------------------END EXECUTE SIGNALS-------------------------------
   
  ------------------MEM ACCESS STAGE SIGNALS-------------------------------

  --intermediate signal for AND gate ON BRANCH TAKEN
  signal s_branch_logic_mux : std_logic_vector(31 downto 0);
  signal s_branch_alu_out   : std_logic_vector(31 downto 0);
  ------------------END MEM ACCESS STAGE SIGNALS-------------------------------


  ------------------WRITE BACK SIGNALS-------------------------------
  signal s_choose_branch_or_jump          : std_logic_vector(31 downto 0);
  signal s_dmem_mux_out                   : std_logic_vector(31 downto 0);
  ------------------END WRITE BACK SIGNALS-------------------------------

  


--------------------------------------------------------------------------------------------------------------------------
begin

  -- TODO: This is required to be your final input to your instruction memory. This provides a feasible method to externally load the memory module which means that the synthesis tool must assume it knows nothing about the values stored in the instruction memory. If this is not included, much, if not all of the design is optimized out because the synthesis tool will believe the memory to be all zeros.
 ------------------MEMORY MODULES-------------------------------
  with iInstLd select
    s_IMemAddr <= s_NextInstAddr when '0',
      iInstAddr when others;


  IMem: mem
    generic map(ADDR_WIDTH => 10,
                DATA_WIDTH => N)
    port map(clk  => iCLK,
             addr => s_IMemAddr(11 downto 2),
             data => iInstExt,
             we   => iInstLd,
             q    => s_Inst);
  
  DMem: mem
    generic map(ADDR_WIDTH => 10,
                DATA_WIDTH => N)
    port map(clk  => iCLK,
             addr => s_DMemAddr(11 downto 2),
             data => s_DMemData,
             we   => s_DMemWr,
             q    => s_DMemOut);

--------------------------------------------------------------------------------------------------------------------------
------------------MULTIPLEXORS-------------------------------
    Rd_Rt_MUXI: mux2t1_N 
      generic map(N => 5) 
                      port map(i_S => s_reg_dst,
                              i_D0 => s_Inst(20 downto 16),
                              i_D1 => s_Inst(15 downto 11), 
                              o_O  =>  s_mux_write_reg_out);

    ALU_MUX: mux2t1_N port map(i_S => s_ALU_src,
                              i_D0 => s_read_data_2_o, --##TODO
                              i_D1 => s_extend_out, 
                              o_O  => s_alu_mux_out);

    -- This is the last mux in the line. s_proper_instruction is the outcome of all the branch, jump, and pc adder logic.
    Jump_MUX: mux2t1_N port map(i_S  => s_jump,
                                i_D0 => s_branch_logic_mux, --##TODO
                                i_D1 => s_jump_logic_out, 
                                o_O  => s_choose_branch_or_jump);
      
      ------------------JUMP AND LINK LOGIC-------------------------------
    --This MUX is in charge of feeding thirty one into the write register port when PC+8 needs to be stored in the RA register. whoose 31 when 1.
    Jal_Wr_MUX: mux2t1_N 
                generic map(N => 5) 
                         port map(i_S  => s_jal,
                                  i_D0 => s_mux_write_reg_out, --##TODO
                                  i_D1 => s_literal_thirty_one, 
                                  o_O  => s_RegWrAddr);

    -- This MUX is in charge of feeding the PC+8 into the write data port when it is needed. choose 1 when PC + 8.
    Jal_Data_MUX: mux2t1_N port map(i_S  => s_choose_PC_plus_eight,
                                    i_D0 => s_dmem_mux_out, --##TODO
                                    i_D1 => s_branch_alu_out, 
                                    o_O  => s_RegWrData);
                                    
    Jal_choose_hard_one_MUX: mux2t1_N port map(i_S  => s_choose_one,  --feed this into the shifter. One shifted twice is four, added to the PC + 4 = PC + 8; choose 1 when 1.
                                               i_D0 => s_extend_out, --##TODO
                                               i_D1 => s_literal_one, 
                                               o_O  => s_jal_cho_out);

    jr_choose_r31: mux2t1_N port map(i_S  => s_jr,  --feed this into the shifter. One shifted twice is four, added to the PC + 4 = PC + 8; choose 1 when 1.
                                     i_D0 => s_choose_branch_or_jump, --##TODO
                                     i_D1 => s_alu_result_out, 
                                     o_O  => s_proper_instruction);
    ------------------END JUMP AND LINK LOGIC-------------------------------


    Dmem_mux: mux2t1_N port map(i_S  => s_mem_to_reg,
                                i_D0 => s_DMemOut, --##TODO
                                i_D1 => s_alu_result_out,  --this is going to be the ALU output
                                o_O  => s_dmem_mux_out); --carries data back
  --------------------------------------------------------------------------------------------------------------------------
  ------------------REGISTER FILE-------------------------------#Done
    Regi_file: registerFile port map (read_reg_1 => s_Inst(25 downto 21), --rs
                                      read_reg_2 => s_Inst(20 downto 16), --rt
                                      i_WE       => s_RegWr, 
                                      CLK => iCLK,
                                      RST => iRST,
                                      i_reg_data => s_RegWrData,
                                      i_writeto_reg => s_RegWrAddr,
                                      o_read_data_1 => s_read_data_1_o, 
                                      o_read_data_2 => s_read_data_2_o);

  ------------------PROGRAM COUNTER-------------------------------#TPDP
    Program_counter: pcreg port map(i_CLK => iCLK,
                                    i_RST => iRST,
                                    i_WE	=> '1', --HERE?? ASK TA
                                    i_D	=> s_proper_instruction,
                                    o_Q	=> s_NextInstAddr);

    ------------------SIGN EXTENDER-------------------------------#Done
    Extender: bit_width_extender port map(control	=> s_sign_extend_select, --sign extension vs zero extension
                                          i_A	    => s_Inst(15 downto 0),
                                          o_F	    => s_extend_out);

    ------------------CONTROL-------------------------------#Done but may need adjustments
    Control_Unit: CONTROLUNIT port map(i_instruction_bits_high  => s_Inst(31 downto 26),
                                       i_instruction_bits_low   => s_Inst(5 downto 0),
                                       o_reg_dst                => s_reg_dst,
                                       o_jump                   => s_jump, 
                                       o_branch                 => s_branch, 
                                       o_mem_to_reg             => s_mem_to_reg,
                                       o_sign_extend_select     => s_sign_extend_select,
                                       o_mem_write              => s_DMemWr,    --data memory write enable signal
                                       o_ALUsrc                 => s_ALU_src,
                                       o_reg_write              => s_RegWr,
                                       o_halt_signal            => s_Halt,
                                       o_jal                    => s_jal,
                                       o_choose_hard_one        => s_choose_one,
                                       o_choose_PC_plus_eight   => s_choose_PC_plus_eight,
                                       o_jr                     => s_jr,
                                       o_ALU_Controller         => s_ALU_control_o);

    ------------------ALU-------------------------------#Done
    ALU_main: ALU port map(i_A           => s_read_data_1_o,
                           i_B           => s_alu_mux_out,
                           result_out    => s_alu_result_out,
                           i_ALU_control => s_ALU_control_o,
                           zero_out      => s_alu_zero_out,
                           o_overflow    => s_Ovfl);

    ------------------ALU CONTROL-------------------------------#Done

    ------------------PC ADDER-------------------------------#Done
    PC_adder: n_bit_adder port map(An_in  => s_NextInstAddr, --address plus four
                                   Bn_in  => s_literal_four,
                                   Cn_in  => s_carry_in_equals_zero,
                                   Cn_out => s_carry_out,
                                   Sn_out => pc_adder_out); --adder out is same signal as PC in

    -- ------------------JUMP LOGIC BLOCK-------------------------------#TODO
    jump_logic: jumpLogic port map(i_instruction_bits =>  s_Inst(25 downto 0),
                                   i_pc_adder_bits    =>  pc_adder_out,
                                   o_F                =>  s_jump_logic_out);


     -- ------------------BRANCH LOGIC BLOCK-------------------------------#TODO
    branch_logic_block: branchLogicBlock port map(i_pc_adder_instruction => pc_adder_out,
                                                  i_sign_extended_num    => s_jal_cho_out,
                                                  i_branch               => s_branch,
                                                  i_alu_zero             => s_alu_zero_out,
                                                  o_alu_result           => s_branch_alu_out,
                                                  o_mux_out_F            => s_branch_logic_mux);
                                                
    oALUOut <= s_alu_result_out;
                            
   
  -- TODO: Ensure that s_Halt is connected to an output control signal produced from decoding the Halt instruction (Opcode: 01 0100)
  -- TODO: Ensure that s_Ovfl is connected to the overflow output of your ALU

  -- TODO: Implement the rest of your processor below this comment! 

end structure;

