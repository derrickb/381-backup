--TEAM G-NULL
--CPRE 381 PROJECT 1 LAB SECTION 6
--Professor Berk

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- entity 
entity instructionDecoder is
    generic(N : integer := 32);
    port(opcode                         : in std_logic_vector(5 downto 0);
         registerDestination            : out std_logic; --selects whether the write register takes the rd or rt value.
         jumpToInstructionAddress       : out std_logic;
         branchToInstructionAddress     : out std_logic;
         memoryToRegisterMux            : out std_logic; --selects whether memory value writes back to register or ALU result does.
         memoryWriteEnable              : out std_logic;
         registerWriteEnable            : out std_logic;
         ALU_sourceRegister             : out std_logic;
         ALU_operation                  : out std_logic_vector(3 downto 0); --A 3 bit number e.g. 000
         signExtendSelect               : out std_logic;
         haltSignalOut                  : out std_logic;
         register31                     : out std_logic
         );
end instructionDecoder;

-- architecture
architecture instructionDecoder_arch of instructionDecoder is 
--componenets
--intermediate signals
signal helperSignal : std_logic_vector(11 downto 0);

begin
    with (opcode) select
    -- #TODO fill in helperSignal outputs; current vals are dummies
    -- helperSignal(11) = register destination 
    -- helperSignal(10) = jump to instruction address
    -- helperSignal(9) = branch to instruction address
    -- helperSignal(8) = memoryToRegisterMux
    -- helperSignal(7) = memory write enable 
    -- helperSignal(6) = register write enable 
    -- helperSignal(5) = sign extend select
    -- helperSignal(4) = alu source register
    -- helperSignal(3 downto 0) = alu operation


    -- NOTE: Need to harcode the register 31 into the register destination mux for the JAL and JR instructions!!
    haltSignalOut <= '1' when "010100",
                  '0' when others;


    with (opcode) select
    helperSignal <= "000101110001" when "001000", --addi
                    "000101110001" when "001001", --addiu #TODO ask about how this differs from addi in terms of implementation.
                    "000101010010" when "001100", --andi
                    "000101011101" when "001111", --lui #TODO this is a shift left op with zeros i.e. xxxx000000.        zero extend
                    "000001110001" when "100011", --lw 
                    "000101010100" when "001110", --xori
                    "000101010011" when "001101", --ori
                    "000101111110" when "001010", --slti; 
                    "000010110001" when "101011", --sw
                    "001000000111" when "000100", --beq; also a subtract operation.
                    "001100001100" when "000101", --bne; also a subtract operation. DONT FORGET ALU WILL NEED LOGIC FOR A ZERO OUPUT.
                    "010000001111" when "000010", --j will have to come back to jump...
                    -- "-------0000" when "000011", --jal #TODO set on no operation rn.
                    -- "-------0000" when "------", --jr #TODO set on no operation rn. this is an r type instruction...
                    "000----1011" when "------", --repl.qb #TODO set on no operation rn.
                    "100101000000" when others;
    
    registerDestination        <= helperSignal(11);
    jumpToInstructionAddress   <= helperSignal(10);
    branchToInstructionAddress <= helperSignal(9);
    memoryToRegisterMux        <= helperSignal(8);
    memoryWriteEnable          <= helperSignal(7);
    registerWriteEnable        <= helperSignal(6);
    signExtendSelect           <= helperSignal(5);
    ALU_sourceRegister         <= helperSignal(4);
    ALU_operation              <= helperSignal(3 downto 0);
    

end instructionDecoder_arch;