--Derrick Brandt
--CPRE 381 Lab 2
--Professor Berk

library IEEE;
use IEEE.std_logic_1164.all;



--entity
entity Nbit_register is 
	generic(N : integer := 32);
	port(i_CLK	: in std_logic;
	     i_RST	: in std_logic;
	     i_WE	: in std_logic;
	     i_D	: in std_logic_vector(N-1 downto 0);
	     o_Q	: out std_logic_vector(N-1 downto 0));
end Nbit_register;



--architecture
architecture Nbit_register_arch of Nbit_register is
--components
component dffg is 
port(i_CLK        : in std_logic;     -- Clock input
     i_RST        : in std_logic;     -- Reset input
     i_WE         : in std_logic;     -- Write enable input
     i_D          : in std_logic;     -- Data value input
     o_Q          : out std_logic);   -- Data value output
end component;
--intermediate signals; Needed?

--begin
begin

	GNBITREG:
		for i in 0 to N-1 generate
		REGI: dffg port map(i_CLK => i_CLK, --SAME CLK, RESET, AND WRITE ENABLE FOR ALL
				    i_RST => i_RST,
				    i_WE  => i_WE,
				    i_D   => i_D(i),
				    o_Q   => o_Q(i));
	end generate GNBITREG;


end Nbit_register_arch;