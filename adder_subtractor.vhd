--Derrick Brandt
--CPRE Lab 1 Section 6
--Professor Berk

library IEEE;
use IEEE.std_logic_1164.all;

--entity
entity adder_subtractor is 
generic(N : integer := 32);
port(A, B		: in std_logic_vector(N-1 downto 0);
     nAdd_Sub		: in std_logic;
	 overflow		: out std_logic;
     Add_Sub_out	: out std_logic_vector(N-1 downto 0));
end adder_subtractor;

-----------------------------------architecture--------------------------------------------
architecture adder_subr_arch of adder_subtractor is


--components

--mux for selecting B or 2's comp B.
component mux2t1_N is
generic(N : integer := 32);

port(i_s          		: in std_logic;
     i_D0         		: in std_logic_vector(N-1 downto 0);
     i_D1         		: in std_logic_vector(N-1 downto 0);
     o_O          		: out std_logic_vector(N-1 downto 0));
end component;

--ones_comp for creating 2's complement of input B
component ones_comp is

	generic(N : integer:= 32);
	port(i_IN		: in std_logic_vector(N-1 downto 0);
     	     o_OUT		: out std_logic_vector(N-1 downto 0));
end component;

--component N_bit adder; used twice. 
component N_bit_adder is 
generic(N : integer := 32); -- Generic of type integer for input/output data width. Default value is 32.
	port (An_in, Bn_in	: in std_logic_vector(N-1 downto 0);
	      Cn_in		: in std_logic;
	      Cn_out		: out std_logic; 
      	      Sn_out		: out std_logic_vector(N-1 downto 0));
end component;

-----------------------intermediate signals-----------------------------------------

--inverter signals
signal s_invg_in, s_invg_out	: std_logic_vector(N-1 downto 0);

--adder signals
signal s_Ain, s_Bin, s_Sout	: std_logic_vector(N-1 downto 0); 
signal s_Cin, s_Cout		: std_logic;

--mux signals
signal s_o			: 	std_logic_vector(N-1 downto 0);
signal s_iS			:	std_logic;


-------------------------------begin-----------------------------------------------
begin
	--invert bits in B; store in s_invg_out
	N_ones_compX:   ones_comp port map(i_IN => B,
					 o_OUT => s_invg_out);

	--Mux contains B(2s comp) in i_D0 and B(regular) in i_D1 
	N_mux2t1: 	mux2t1_N  port map(i_D0 => B,
				    i_D1 => s_invg_out,
				    i_s  => nAdd_Sub,
				    o_O  => s_o);
	
	

	--Adder unit which stores A+B or A-B
	N_bit_adderY: N_bit_adder port map(An_in => A,
									   Bn_in => s_o,
									   Cn_in => nAdd_Sub, --Use 1 as carry in to complete 2s complement
									   Cn_out => overflow, --not sure here;
									   Sn_out => Add_Sub_out);
	s_Cin <= '0';

end adder_subr_arch;