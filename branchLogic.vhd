-- Team G-Null

-- #README -- THIS IS FOR THE LOGIC HOUSED INSIDE THE ALU. DON'T MISTAKE WITH BRANCHLOGICBLOCK;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--entity 
entity branchLogic is 
--generic
port (i_select          : in std_logic;
      add_sub_result    : in std_logic_vector(31 downto 0);
      zero_out          : out std_logic);
end branchLogic;

--architecture
architecture branchLogic_arch of branchLogic is

--components
--[]

--signals
signal helperSignal : std_logic;

-- begin 
begin
        --zero output selector; for branching, if the output from the subtraction operation is zero, then A=B & zero_out should be set high.
        
        with (add_sub_result) select
        helperSignal <= '1' when x"00000000",
                        '0' when others;

        -- when i_select = 0 : beq
        -- when i_select = 1 : bne 
        -- process1: process(i_select, zero_out) is
        -- begin 
        --     if (i_select = '0') then
        --         zero_out <= helperSignal;
        --     else
        --         zero_out <= (NOT helperSignal);
        --     end if;
        -- end process process1;

        with (i_select) select
        zero_out <= helperSignal when '0',
                    (NOT helperSignal) when others;
            
                
end branchLogic_arch; 


-- calculate overflow by XORing your input carry and carry output