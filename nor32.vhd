-- Team G-Null

library IEEE;
use IEEE.std_logic_1164.all;

entity nor32 is

  port(i_A          : in std_logic_vector(31 downto 0);
       i_B          : in std_logic_vector(31 downto 0);
       o_O          : out std_logic_vector(31 downto 0));

end nor32;

architecture dataflow of nor32 is
begin

  o_O <= i_A nor i_B;
  
end dataflow;
