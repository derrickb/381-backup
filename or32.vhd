-- Team G NULL

library IEEE;
use IEEE.std_logic_1164.all;

entity or32 is

  port(i_A          : in std_logic_vector(31 downto 0);
       i_B          : in std_logic_vector(31 downto 0);
       o_O          : out std_logic_vector(31 downto 0));

end or32;

architecture dataflow of or32 is
begin

  o_O <= i_A or i_B;
  
end dataflow;
