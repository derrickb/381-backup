-- CPRE 381 Team GNULL

library IEEE;
use IEEE.std_logic_1164.all;

entity and32 is

  port(i_A          : in std_logic_vector(31 downto 0);
       i_B          : in std_logic_vector(31 downto 0);
       o_O          : out std_logic_vector(31 downto 0));

end and32;

architecture dataflow of and32 is
begin

  o_O <= i_A and i_B;
  
end dataflow;
