-- Team G-Null

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Still used for regular slt

--entity 
entity sltiLogic is 
--generic
port (i_A                      : in std_logic_vector(31 downto 0);
      i_B                      : in std_logic_vector(31 downto 0);
      one_or_zero_out          : out std_logic_vector(31 downto 0));
end sltiLogic;

--architecture
architecture sltiLogic_arch of sltiLogic is

--components

--signals

-- begin 
begin
        --zero output selector; for branching, if the output from the subtraction operation is zero, then A=B & zero_out should be set high.
    
        -- when i_select = 0 : beq
        -- when i_select = 1 : bne 
        process1: process(i_A, i_B) is
        begin 
            if (i_A < i_B) then
                one_or_zero_out <= x"00000001";
            else
                one_or_zero_out <= x"00000000";
            end if;
        end process process1;

end sltiLogic_arch; 


-- calculate overflow by XORing your input carry and carry output