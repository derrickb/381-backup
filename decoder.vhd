--Derrick Brandt
--CPRE 381 Lab 2
--Professor Berk

library IEEE;
use IEEE.std_logic_1164.all;

--Decoder

--entity 
entity decoder is
generic(N   : integer   := 32);

port(D_IN   : in std_logic_vector(4 downto 0);
     i_WE	: in std_logic;
     S_OUT   : out std_logic_vector(N-1 downto 0));
end decoder;

--architecture
architecture decoder_arch of decoder is
--components

--intermediate signals
signal concat_WED    : std_logic_vector(5 downto 0);

--& concatenation operator C_val <= A_val & "00"

--begin
begin 

    concat_WED <= (D_in & i_WE); 

    with (concat_WED) select
    S_OUT <=
             x"00000002" when "000011", -- value 1 corresponds to bit 1
             x"00000004" when "000101",
             x"00000008" when "000111",

             x"00000010" when "001001",
             x"00000020" when "001011",
             x"00000040" when "001101",
             x"00000080" when "001111", --7
         

             x"00000100" when "010001", --one hot
             x"00000200" when "010011",
             x"00000400" when "010101",
             x"00000800" when "010111",

             x"00001000" when "011001",
             x"00002000" when "011011",
             x"00004000" when "011101",
             x"00008000" when "011111",

            ----------------------------

             x"00010000" when "100001", --16
             x"00020000" when "100011",
             x"00040000" when "100101",
             x"00080000" when "100111",

             x"00100000" when "101001", --20
             x"00200000" when "101011",
             x"00400000" when "101101", --22
             x"00800000" when "101111", --23


             x"01000000" when "110001", --24
             x"02000000" when "110011",
             x"04000000" when "110101",
             x"08000000" when "110111",

             x"10000000" when "111001", --28
             x"20000000" when "111011",
             x"40000000" when "111101",
             x"80000000" when "111111", --31
	     x"00000000" when others;

end decoder_arch;