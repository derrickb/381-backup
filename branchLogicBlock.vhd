--TEAM G-NULL
--CPRE 381 PROJECT 1 LAB SECTION 6
--Professor Berk

--ALU entity and architecture

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--entity
entity branchLogicBlock is 
    port(i_pc_adder_instruction : in std_logic_vector(31 downto 0);
         i_sign_extended_num    : in std_logic_vector(31 downto 0);
         i_branch               : in std_logic;
         i_alu_zero             : in std_logic;
         o_alu_result           : out std_logic_vector(31 downto 0);
         o_mux_out_F            : out std_logic_vector(31 downto 0));

end branchLogicBlock;

--architecture
architecture branchLogicBlock_arch of branchLogicBlock is 

--components
component mux2t1_N is
    generic(N : integer := 32); -- Generic of type integer for input/output data width. Default value is 32.
    port(i_S          : in std_logic;
        i_D0         : in std_logic_vector(N-1 downto 0);
        i_D1         : in std_logic_vector(N-1 downto 0);
        o_O          : out std_logic_vector(N-1 downto 0));

  end component;
  
component n_bit_adder is 
    generic(N : integer := 32); -- Generic of type integer for input/output data width. Default value is 32.
    port (An_in, Bn_in	: in std_logic_vector(N-1 downto 0);
          Cn_in		: in std_logic;
          Cn_out		: out std_logic; 
          Sn_out		: out std_logic_vector(N-1 downto 0));
end component;

component shiftLeft2 is 
    generic(N   : integer := 26);
    port(i_A    : in std_logic_vector(N-1 downto 0);
        o_F    : out std_logic_vector(N-1 downto 0));
end component;

component andg2 is 
port(i_A          : in std_logic;
     i_B          : in std_logic;
     o_F          : out std_logic);
end component;
--signals

signal s_andg2_out            : std_logic;
signal shiftLeft2_out         : std_logic_vector(31 downto 0);
signal s_carry_in_equals_zero : std_logic := '0';
signal alu_result_out         : std_logic_vector(31 downto 0);
signal s_carry_out            : std_logic;

--begin
    begin   

     ------------------AND GATE-------------------------------#Done
     Branch_andg2: andg2 port map(i_A => i_branch,
                                  i_B => i_alu_zero,
                                  o_F => s_andg2_out);

        ------------------PC ADDER-------------------------------#Done
    ALU_adder: n_bit_adder port map(An_in  => i_pc_adder_instruction, --address plus four
                                    Bn_in  => shiftLeft2_out,
                                    Cn_in  => s_carry_in_equals_zero,
                                    Cn_out => s_carry_out,
                                    Sn_out => alu_result_out); --adder out is same signal as PC in

    shifty: shiftLeft2 
    generic map(N => 32) 
        port map(i_A => i_sign_extended_num,
                o_F => shiftLeft2_out);

    Branch_MUX: mux2t1_N port map(i_S  => s_andg2_out,
                                  i_D0 => i_pc_adder_instruction, --##TODO
                                  i_D1 => alu_result_out, 
                                  o_O  => o_mux_out_F);

    o_alu_result <= alu_result_out;




end branchLogicBlock_arch;



