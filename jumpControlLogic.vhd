--TEAM G-NULL
--CPRE 381 PROJECT 1 LAB SECTION 6
--Professor Berk

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- entity 
entity jumpControlLogic is
    generic(N : integer := 32);
    port(opcode                           : in std_logic_vector(5 downto 0); --selects whether the write register takes the rd or rt value.
         o_jal                            : out std_logic;
         o_choose_hard_one                : out std_logic;
         o_choose_PC_plus_eight           : out std_logic;
         o_jr                             : out std_logic;
         i_enable                         : in std_logic
         );
end jumpControlLogic;

architecture jumpControlLogic_arch of jumpControlLogic is

--components

--signals
signal s_helperSignal : std_logic_vector(3 downto 0);
signal s_opcode_concat_enable : std_logic_vector(6 downto 0);
--begin
    begin 
    -- helperSignal(3) = s_jal
    -- helperSignal(2) = s_choose_hard_one
    -- helperSignal(1) = s_choose_PC_plus_eight
    -- helperSignal(0) = s_jr

    s_opcode_concat_enable <= (opcode & i_enable);

    with (s_opcode_concat_enable) select
    s_helperSignal <= "1110" when "0000111", --jal
                      "0001" when "0010001", --jr
                      "0000" when others; --this takes care of j
    
    o_jal                   <= s_helperSignal(3);
    o_choose_hard_one       <= s_helperSignal(2);
    o_choose_PC_plus_eight  <= s_helperSignal(1);
    o_jr                    <= s_helperSignal(0);

end jumpControlLogic_arch;