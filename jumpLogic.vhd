-- Team G-Null

--Logic necessary to perform jumps

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--entity 

entity jumpLogic is 
generic(N   : integer   := 26);
port(i_instruction_bits : in std_logic_vector(N-1 downto 0);
     i_pc_adder_bits    : in std_logic_vector(N + 5 downto 0);
     o_F                : out std_logic_vector(N + 5 downto 0));
end jumpLogic;

--architecture
architecture jumpLogic_arch of jumpLogic is 
--components
component shiftLeft2 is 
    generic(N   : integer := 26);
    port(i_A    : in std_logic_vector(N-1 downto 0);
        o_F    : out std_logic_vector(N-1 downto 0));
end component;

--signals
signal s_pc_adder_helper    : std_logic_vector(3 downto 0);
signal s_shift_helper_out   : std_logic_vector(N-1 downto 0);
--begin
    begin
        --the base logic involves shifting then concatenating signals
        s_pc_adder_helper <= i_pc_adder_bits(31 downto 28);

        shifty: shiftLeft2 port map(i_A => i_instruction_bits,
                                    o_F => s_shift_helper_out);
        

        o_F <= (s_pc_adder_helper & '0' & '0' & s_shift_helper_out);
        
        




end jumpLogic_arch;