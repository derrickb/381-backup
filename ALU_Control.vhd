--TEAM G NULL
--CPRE 381 PROJECT 1 
--Professor Berk

--ALU Control Unit

--2^6 potential funct codes from i_instruction_bits
--We have 27 total instructions to implement
--Every instruction except jump uses the ALU(so 26 instructions)
--Will likely need a multiplexor for selection
--Not sure yet what we will use for the funct code outputs. Copy MIPS green sheet?
--Going to realistically have 3-4 bit ALU OP

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


--entity
entity ALU_Control is 
port(i_instruction_bits      : in std_logic_vector(5 downto 0); --i_instruction_bits = OPcode on MIPS green sheet.
     i_ALU_Op                : in std_logic_vector(3 downto 0);
     o_ALU_Control           : out std_logic_vector(3 downto 0));
end ALU_Control;

--architecture
architecture ALU_Control_Arch of ALU_Control is 
--components
     
--intermediate signals

--begin
begin
     -- 12 R type operations that need to be implemented
     with (i_instruction_bits) select
          o_ALU_Control <= "0001" when "100000", --add
                           "0001" when "100001", --addu --DO WE NEED SOMETHING DIFFERENT FOR UNSIGNED ADDITION
                           "0010" when "100100", --and
                           "0010" when "100111", --nor
                           "0101" when "100110", --xor
                           "0011" when "100101", --or
                           "0111" when "101010", --slt
                           "1000" when "000000", --sll
                           "0110" when "000010", --srl
                           "1010" when "000011", --sra
                           "0111" when "100010", --sub
                           "0111" when "100011", --subu --DO WE NEED SOMETHING DIFFERENT FOR UNSIGNED SUBTRACTION
                           "0001" when "001000", -- for jr adding; assuming that rt register = register 0
                           i_ALU_Op when others;

     
end ALU_Control_Arch;