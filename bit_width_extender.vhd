--Derrick Brandt
--CPRE 381 LAB 2 SECTION 6
--Professor Berk

library IEEE;
use IEEE.std_logic_1164.all;

--entity
entity bit_width_extender is 
generic(N : integer := 32);
port(control	: in std_logic;
     i_A	: in std_logic_vector(15 downto 0);
     o_F	: out std_logic_vector(N-1 downto 0));
end bit_width_extender;

--architecture
architecture bit_width_extender_arch of bit_width_extender is 

--components
constant zero_extend	: std_logic_vector := "0000000000000000";
constant one_extend	: std_logic_vector := "1111111111111111";

--signals
signal concat_with_zeros	: std_logic_vector(31 downto 0);
signal concat_with_self		: std_logic_vector(31 downto 0);

--begin
begin 
	concat_with_zeros <= (zero_extend & i_A);

	with(i_A(15)) select
	concat_with_self <= (one_extend & i_A) when '1',
						(zero_extend & i_A) when '0',
						x"00000000" when others;

	with (control) select 
	o_F <= (concat_with_zeros) when '0',
	       (concat_with_self) when  '1',
	       x"00000000" when others;
		
end bit_width_extender_arch;