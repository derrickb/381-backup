--Derrick Brandt
--CPRE 381 Lab 1 Section 6
--Professor Berk

library IEEE;
use IEEE.std_logic_1164.all;

--entity
entity n_bit_adder is 
	generic(N : integer := 32); -- Generic of type integer for input/output data width. Default value is 32.
	port (An_in, Bn_in	: in std_logic_vector(N-1 downto 0);
	      Cn_in		: in std_logic;
	      Cn_out		: out std_logic; 
      	  Sn_out		: out std_logic_vector(N-1 downto 0));
end n_bit_adder;

--architecture
architecture n_bit_adder_arch of n_bit_adder is
--component
component my_adder is 
	port (A_in, B_in, C_in		: in std_logic;
      	      C_out, S_out		: out std_logic);
end component;


--intermediate signals
signal s_carry	: std_logic_vector(N-1 downto 0); --used to transfer carry out bit to carry in bit


--begin
begin 
	
	adder_bit_zero: my_adder port map(A_in => An_in(0),
				          B_in => Bn_in(0),
					  C_in => Cn_in,
				          C_out => s_carry(0),
				          S_out => Sn_out(0));
	
	G_NBit_Adder:	
		for i in 1 to N-1 generate
		ADDI: my_adder port map(A_in => An_in(i), 
					B_in => Bn_in(i),
					C_in => s_carry(i-1),
					C_out => s_carry(i),
					S_out => Sn_out(i));
		end generate G_NBit_Adder;

	Cn_out <= s_carry(N-1); 
	
end n_bit_adder_arch;