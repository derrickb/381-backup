--Derrick Brandt
--CPRE 381 Lab 1 Section 6
--Professor Berk

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;  -- For logic types I/O
library std;
use std.env.all;                -- For hierarchical/external signals
use std.textio.all;             -- For basic I/O

--entity
entity tb_ones_comp is 
	generic(N : integer := 32; gCLK_HPER   : time := 10 ns);
end tb_ones_comp;

--architecture
architecture tb_ones_comp_arch of tb_ones_comp is
component ones_comp is

	generic(N : integer:= 32);
	port(i_IN		: in std_logic_vector(N-1 downto 0);
     	     o_OUT		: out std_logic_vector(N-1 downto 0));
end component;

--signals
signal s_i, s_o		: std_logic_vector(N-1 downto 0);

begin
	DUT0: ones_comp port map(i_IN => s_i,
				 o_OUT => s_o);

	P_TESTCASES: process
	begin
		s_i <= x"00000000";

		wait for 100 ns;

		s_i <= x"11111111";
		
		wait for 100 ns;

		s_i <= x"10100001";


		wait;
end process;


end architecture;
