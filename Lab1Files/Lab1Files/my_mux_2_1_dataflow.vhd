--Derrick Brandt
--CPRE 381 LAB 1 SECTION 6
--Professor Berk

library IEEE;
use IEEE.std_logic_1164.all;

entity my_mux_2_1_dataflow is 

	port (i_D0, i_D1, i_s	:in	std_logic;
              o_O		:out	std_logic);
end my_mux_2_1_dataflow;

architecture my_mux_2_1_dataflow_arch of my_mux_2_1_dataflow is
--intermediate signal declaration
	signal d0_out, d1_out	: std_logic;
--begin
begin
	o_O <= i_D0 when (i_s = '0') else
	       i_D1 when (i_s = '1') else
	       '0';

end my_mux_2_1_dataflow_arch;