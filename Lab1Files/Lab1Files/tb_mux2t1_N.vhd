--Derrick Brandt
--CPRE 381 Lab 1 Section 6
--Professor Berk

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;  -- For logic types I/O
library std;
use std.env.all;                -- For hierarchical/external signals
use std.textio.all;             -- For basic I/O

--entity
entity tb_mux2t1_N is
	generic(N : integer := 32; gCLK_HPER   : time := 10 ns);
end tb_mux2t1_N;


--architecture
architecture tb_mux2t1_N_arch of tb_mux2t1_N is
--component

component mux2t1_N is
generic(N : integer := 32);

port(i_s          : in std_logic;
     i_D0         : in std_logic_vector(N-1 downto 0);
     i_D1         : in std_logic_vector(N-1 downto 0);
     o_O          : out std_logic_vector(N-1 downto 0));
end component;

--signals
signal s_iD0, s_iD1, s_o	: 	std_logic_vector(N-1 downto 0);
signal s_iS			:	std_logic;

begin
	DUT0: mux2t1_N port map(i_D0 => s_iD0,
				i_D1 => s_iD1,
				i_s  => s_iS,
				o_O  => s_o);

	P_TESTCASES: process
	begin
		s_iD0 <= x"00000000";
		s_iD1 <= x"00000001";
		s_iS <= '1';

		wait for 100 ns;

		s_iD0 <= x"00000001";
		s_iD1 <= x"00000000";
		s_iS <= '0';
		
		wait for 100 ns;

		s_iD0 <= x"00000000";
		s_iD1 <= x"00000000";
		s_iS <= '0';


		wait;
end process;

end tb_mux2t1_N_arch;
