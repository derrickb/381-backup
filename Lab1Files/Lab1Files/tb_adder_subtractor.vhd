--Derrick Brandt
--Lab 1 Section 6 CPRE 381
--Professor Berk

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;  -- For logic types I/O
library std;
use std.env.all;                -- For hierarchical/external signals
use std.textio.all;             -- For basic I/O

--entity

entity tb_adder_subtractor is
	generic(N : integer := 32; gCLK_HPER   : time := 10 ns);
end tb_adder_subtractor;

--architecture

architecture tb_adder_subtractor_arch of tb_adder_subtractor is
--components
component adder_subtractor is

generic(N : integer := 32);
port(A, B		: in std_logic_vector(N-1 downto 0);
     nAdd_Sub		: in std_logic;
     Add_Sub_out	: out std_logic_vector(N-1 downto 0));

end component;

--intermediate signals
signal s_Ain, s_Bin, s_AS_out	: std_logic_vector(N-1 downto 0);
signal sn_add_sub	: std_logic;

--begin
begin
	DUT0: adder_subtractor port map(A => s_Ain,
					B => s_Bin,
					nAdd_Sub => sn_add_sub,
					Add_Sub_out => s_AS_out);

	P_TESTCASES: process
	begin

		s_Ain <= x"0000FFFF";
		s_Bin <= x"00002222"; --Expected output: 0001 2221
		sn_add_sub <= '0'; 
		
		wait for 100 ns;
		
		s_Ain <= x"00000007";
		s_Bin <= x"00000006"; --Expected output: 0000 0001
		sn_add_sub <= '1'; 

		wait for 100 ns;

		s_Ain <= x"01010101"; --Expected output: ABABAB ABAB
		s_Bin <= x"aaaaaaaa";
		sn_add_sub <= '0'; 
		
		wait;
	end process;

end tb_adder_subtractor_arch;