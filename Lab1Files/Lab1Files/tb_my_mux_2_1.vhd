---Derrick Brandt
---CPRE 381 Lab Section 6
---09-10-2021

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;  -- For logic types I/O
library std;
use std.env.all;                -- For hierarchical/external signals
use std.textio.all;             -- For basic I/O


--Entity Declaration
entity tb_my_mux_2_1 is
	generic(gCLK_HPER   : time := 10 ns); 
end tb_my_mux_2_1;

--Architecture
architecture tb_mux_2_1_architecture of tb_my_mux_2_1 is

-- Define the total clock period time
constant cCLK_PER  : time := gCLK_HPER * 2;

--componenet declaration
component my_mux_2_1 is
	port(i_D0, i_D1, i_s	: in std_logic;
     	     o_O		: out std_logic);
end component;

--signals

signal s_iD0, s_iD1, s_iS, s_0	: std_logic;
--begin
begin
	DUT0:	my_mux_2_1 port map(i_D0 => s_iD0,
				    i_D1 => s_iD1,
				    i_s	=> s_iS,
				    o_O => s_0);

	P_TESTCASES: process
	begin
		s_iD0 <= '0';
		s_iD1 <= '1';
		s_iS <= '1';

		wait for 100 ns;

		s_iD0 <= '1';
		s_iD1 <= '0';
		s_iS <= '0';
		
		wait for 100 ns;

		s_iD0 <= '0';
		s_iD1 <= '1';
		s_iS <= '0';


		wait;

end process;
		
		
end tb_mux_2_1_architecture;