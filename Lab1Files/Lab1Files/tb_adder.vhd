--Derrick Brandt
--CPRE 381 Lab 1 Section 6
--Professor Berk

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;  -- For logic types I/O
library std;
use std.env.all;                -- For hierarchical/external signals
use std.textio.all;             -- For basic I/O


--entity
entity tb_adder is 
	generic(gCLK_HPER   : time := 10 ns);
end tb_adder;

--architecture
architecture tb_adder_arch of tb_adder is

-- Define the total clock period time
constant cCLK_PER  : time := gCLK_HPER * 2;

component my_adder is 
	port (A_in, B_in, C_in		: in std_logic;
      	      C_out, S_out		: out std_logic);
end component;

--signals
signal s_Ain, s_Bin, s_Cin, s_Cout, s_Sout	: std_logic; 

--begin
begin 
	DUT0: my_adder port map(A_in => s_Ain,
			     B_in => s_Bin,
			     C_in => s_Cin,
			     C_out => s_Cout,
			     S_out => s_Sout);
P_TESTCASES: process
	begin
		s_Ain <= '0';
		s_Bin <= '1';
		s_Cin <= '0';
		

		wait for 100 ns;
		
		s_Ain <= '1';
		s_Bin <= '0';
		s_Cin <= '0';
		

		wait for 100 ns;

		s_Ain <= '0';
		s_Bin <= '1';
		s_Cin <= '1';
		
		wait for 100 ns;

		s_Ain <= '1';
		s_Bin <= '1';
		s_Cin <= '1';

		wait for 100 ns;

		s_Ain <= '0';
		s_Bin <= '0';
		s_Cin <= '0';

		wait;

	end process;



end tb_adder_arch;