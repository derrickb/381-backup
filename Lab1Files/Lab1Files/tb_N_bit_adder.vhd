--Derrick Brandt
--CPRE Lab 1 Section 6
--Professor Berk


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;  -- For logic types I/O
library std;
use std.env.all;                -- For hierarchical/external signals
use std.textio.all;             -- For basic I/O

--entity

entity tb_N_bit_adder is 
	generic(N : integer := 32; gCLK_HPER   : time := 10 ns);
end tb_N_bit_adder;

--architecture
architecture tb_N_bit_adder_arch of tb_N_bit_adder is




--component
component N_bit_adder is 
generic(N : integer := 32); -- Generic of type integer for input/output data width. Default value is 32.
	port (An_in, Bn_in	: in std_logic_vector(N-1 downto 0);
	      Cn_in		: in std_logic;
	      Cn_out		: out std_logic; 
      	      Sn_out		: out std_logic_vector(N-1 downto 0));
end component;





--signals
signal s_Ain, s_Bin, s_Sout	: std_logic_vector(N-1 downto 0); 
signal s_Cin, s_Cout		: std_logic;





--begin
begin
	DUT0: N_bit_adder port map(An_in  => s_Ain,
				   Bn_in  => s_Bin,
				   Cn_in  => s_Cin,
				   Cn_out => s_Cout,
				   Sn_out => s_Sout);
	
	P_TESTCASES: process
	begin

		s_Ain <= x"00000000";
		s_Bin <= x"00002222"; --Expected output: 63164895
		s_Cin <= '0'; 
		
		wait for 100 ns;
		
		s_Ain <= x"0FFFFFFF";
		s_Bin <= x"00000011"; --Expected output: 10000010
		s_Cin <= '0';

		wait for 100 ns;

		s_Ain <= x"01010101"; --Expected output: 5f3f823e
		s_Bin <= x"aaaaaaaa";
		s_Cin <= '0';
		
		wait for 100 ns;

		s_Ain <= x"2850f5df";
		s_Bin <= x"9aa8a3aa"; --Expected output: C2F9998A
		s_Cin <= '1';

		wait for 100 ns;

		s_Ain <= x"53af6f37"; --Expected output: D39F64ED
		s_Bin <= x"7feff5b6";
		s_Cin <= '0'; 

		wait for 100 ns;

		s_Ain <= x"F0000000";
		s_Bin <= x"10000000"; --Expected output: 00000000 Carry: 1
		s_Cin <= '0';
		
		wait for 100 ns;
		
		s_Ain <= x"0F0000a0";
		s_Bin <= x"010000a0"; --Expected output: 10000140
		s_Cin <= '0';

		wait;

	end process;

end tb_N_bit_adder_arch;