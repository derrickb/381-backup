##Derrick Brandt
##CPRE 381 Lab 2 Section 6
##10/02/21
##Professor Berk

##Test Bench MIPS_Processor File

#COMPILE ALL FILES

vcom -reportprogress 300 -work work {/home/derrickb/derrick_b/CPRE 381/Project1/Canvas Downloads/src/Lab2Files/MUX32Package.vhd} -2008
vcom -reportprogress 300 -work work {/home/derrickb/derrick_b/CPRE 381/Project1/Canvas Downloads/src/Lab2Files/*.vhd} -2008
vcom -reportprogress 300 -work work {/home/derrickb/derrick_b/CPRE 381/Project1/Canvas Downloads/src/mux2t1_N.vhd} -2008
vcom -reportprogress 300 -work work {/home/derrickb/derrick_b/CPRE 381/Project1/Canvas Downloads/src/Lab1Files/Lab1Files/*.vhd} -2008
vcom -reportprogress 300 -work work {/home/derrickb/derrick_b/CPRE 381/Project1/Canvas Downloads/src/*.vhd} -2008

#LAUNCH simulation
vsim work.tb_mips_processor

#ADD waves
add wave -position insertpoint sim:/tb_mips_processor/DUTMIPSPROC/*

#log command for adding future waves
log -r *


mem load -infile dmem2.hex -format hex /tb_my_second_datapath/DUTSECONDPATH/memory/ram