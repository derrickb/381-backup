--Derrick Brandt
--CPRE 381 Lab 2 Section 6
--Professor Berk

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;  -- For logic types I/O
library std;
use std.env.all;                -- For hierarchical/external signals
use std.textio.all;             -- For basic I/O

--entity
entity tb_register_file is 
generic(N : integer := 32; gCLK_HPER   : time := 50 ns);
end tb_register_file;

--architecture
architecture tb_register_file_arch of tb_register_file is


--VARIABLE
constant cCLK_PER  : time := gCLK_HPER * 2;

--components
component registerFile is 
generic(N : integer := 32);
port (read_reg_1, read_reg_2	: in std_logic_vector(4 downto 0);
      i_WE				: in std_logic;
      CLK				: in std_logic;
      RST				: in std_logic;
      i_reg_data		: in std_logic_vector(31 downto 0);
      i_writeto_reg		: in std_logic_vector(4 downto 0);
      o_read_data_1, o_read_data_2 : out std_logic_vector(31 downto 0));
end component;
--signals
signal s_read_reg_1, s_read_reg_2, s_writeto_reg	: std_logic_vector(4 downto 0);
signal s_reg_data									: std_logic_vector(31 downto 0);
signal sCLK, reset, si_WE : std_logic := '0';
--begin
begin 
	DUTREGFILE: registerFile port map(read_reg_1 => s_read_reg_1,
									  read_reg_2 => s_read_reg_2,
									  i_WE		 => si_WE,
									  CLK		 => sCLK,
									  RST		 => reset, 
									  i_reg_data => s_reg_data,
									  i_writeto_reg => s_writeto_reg);

	P_CLK: process
  		begin
    		sCLK <= '1';          -- clock starts at 1
    		wait for gCLK_HPER; 	-- after half a cycle
    		sCLK <= '0';          -- clock becomes a 0 (negative edge)
    		wait for gCLK_HPER;     -- after half a cycle, process begins evaluation again
  	end process;

	P_RST: process
  	 begin
  		reset <= '0';   
    	 wait for gCLK_HPER/2;
		reset <= '1';
   	 wait for gCLK_HPER*2;
		reset <= '0';
		wait;
  	 end process;  

	P_TESTCASES: process
	begin
		wait for gCLK_HPER*2; -- for waveform clarity, I prefer not to change inputs on clk edges

		s_writeto_reg <= "00011";
		s_reg_data <= x"0000FFFF";
		si_WE <= '1'; 
		s_read_reg_1 <= "00000";
		
		wait for gCLK_HPER*2;
		
		s_writeto_reg <= "00101";
		s_reg_data <= x"00000007";
		si_WE <= '1'; 
		s_read_reg_1 <= "00011";
		s_read_reg_2 <= "00101";

		wait for gCLK_HPER*2;

		s_writeto_reg <= "01001";
		s_reg_data <= x"01010101"; 
		si_WE <= '1'; 
		s_read_reg_1 <= "00011";
		s_read_reg_2 <= "01001";

		wait for gCLK_HPER*2;

	end process;

end tb_register_file_arch;