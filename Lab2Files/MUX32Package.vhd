--Derrick Brandt
--CPRE 381 Lab 2 Section 6
--Professor Berk

--32 to 1 MUX ARRAY
--compile before everything else
library IEEE;
use IEEE.std_logic_1164.all;

package MUX32 is
type muxarray is array (31 downto 0) of std_logic_vector(31 downto 0);
end package MUX32;