--Derrick Brandt
--CPRE 381 Lab 2 Section 6
--Professor Berk

--TEST BENCH 32:1 MUX

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;  -- For logic types I/O
library std;
use std.env.all;                -- For hierarchical/external signals
use std.textio.all;             -- For basic I/O
use IEEE.numeric_std;

--MUX Package
library work;
use work.MUX32.all;

--entity
entity tb_MUX_32_One is 
end tb_MUX_32_One;

--architecture
architecture tb_MUX_32_One_Arch of tb_MUX_32_One is 
--components
component MUX_32_One
port (i_S	: in std_logic_vector(4 downto 0);
      i_D0	: in muxarray;
      S_0	: out std_logic_vector(31 downto 0));
end component;
--intermediate signals
signal si_S	: std_logic_vector(4 downto 0);
signal si_D0	: muxarray;
signal sS_0	: std_logic_vector(31 downto 0);
--begin

--TODO generate 32 bit value for each 32 bit bus
--create test cases

begin --generate statement???

	DUT32MUX: MUX_32_One port map(i_S  => si_S,
				      			  i_D0 => si_D0);
	P_TESTCASES: process
	begin
		--test cases
	si_D0(0) <= x"00000002";
	si_D0(1) <= x"00000002";
	si_D0(2) <= x"00000002";
	si_D0(3) <= x"00000002";
	si_D0(4) <= x"00000002";
	si_D0(5) <= x"00000002";
	si_D0(6) <= x"00000002";
	si_D0(7) <= x"00000002";

	si_D0(8) <= x"00000012";
	si_D0(9) <= x"00000012";
	si_D0(10) <= x"00000012";
	si_D0(11) <= x"00000012";
	si_D0(12) <= x"00000012";
	si_D0(13) <= x"00000012";
	si_D0(14) <= x"00000012";
	si_D0(15) <= x"00000012";
	
	si_D0(16) <= x"00000B12";
	si_D0(17) <= x"00000B12";
	si_D0(18) <= x"00000B12";
	si_D0(19) <= x"00000B12";
	si_D0(20) <= x"00000B12";
	si_D0(21) <= x"00000B12";
	si_D0(22) <= x"00000B12";
	si_D0(23) <= x"00000B12";

	si_D0(24) <= x"0000F112";
	si_D0(25) <= x"0000F112";
	si_D0(26) <= x"0000F112";
	si_D0(27) <= x"0000F112";
	si_D0(28) <= x"0000F112";
	si_D0(29) <= x"0000F112";
	si_D0(30) <= x"0000F112";
	si_D0(31) <= x"0000F112";
		
		wait for 100 ns;

		si_S <= "00000";

		wait for 100 ns;

		si_S <= "01000";
		
		wait for 100 ns;

		si_S <= "10000";
		
		wait for 100 ns;
		
		si_S <= "11101";
	
		wait;
end process;

end tb_MUX_32_One_Arch;