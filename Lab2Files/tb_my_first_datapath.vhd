--Derrick Brandt
--CPRE 381 Lab 2 Section 6
--Professor Berk

--Test Bench for my first datapath
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;  -- For logic types I/O
library std;
use std.env.all;                -- For hierarchical/external signals
use std.textio.all;             -- For basic I/O

--entity
entity tb_my_first_datapath is 
generic(N : integer := 32; gCLK_HPER   : time := 50 ns);
end tb_my_first_datapath;

--architecture
architecture tb_my_first_datapath_arch of tb_my_first_datapath is

--VARIABLE
constant cCLK_PER  : time := gCLK_HPER * 2;

--components
component my_first_datapath is 
port(i_read_rs, i_read_rt, i_write_rd 	: in std_logic_vector(4 downto 0);
     ALUsc	                         : in std_logic;
     neAdd_Sub	                         : in std_logic;
     i_WE                               : in std_logic;
     i_CLK                              : in std_logic;
     i_RST                              : in std_logic;
     i_immediate                        : in std_logic_vector(31 downto 0));
end component;

--signals
signal s_rs, s_rt, s_rd	: std_logic_vector(4 downto 0);
signal sCLK, reset, si_WE : std_logic := '0';
signal s_ALU_sc, sn_add_sub	: std_logic;
signal s_immediate            : std_logic_vector(31 downto 0);

--begin
begin
	DUTDATPATH: my_first_datapath port map(i_read_rs   => s_rs,
					                   i_read_rt   => s_rt,
					                   i_write_rd  => s_rd,
					                   ALUsc       => s_ALU_sc,
                                            neAdd_Sub   => sn_add_sub,
                                            i_WE        => si_WE,
                                            i_CLK       => sCLK,
                                            i_RST       => reset,
                                            i_immediate => s_immediate);

     P_CLK: process
  		begin
    		sCLK <= '1';          -- clock starts at 1
    		wait for gCLK_HPER; 	-- after half a cycle
    		sCLK <= '0';          -- clock becomes a 0 (negative edge)
    		wait for gCLK_HPER;     -- after half a cycle, process begins evaluation again
  	end process;

     P_RST: process
  	 begin
  		reset <= '0';   
    	 wait for gCLK_HPER/2;
		reset <= '1';
   	 wait for gCLK_HPER*2;
		reset <= '0';
		wait;
  	 end process;  

     
     P_TESTCASES: process
	begin
          -- reset <= '1';
          -- wait for gCLK_HPER*2;
          -- reset <= '0';

		wait for gCLK_HPER*2; -- for waveform clarity, I prefer not to change inputs on clk edges

		s_rd <= "00001";                   -- #Place 1 in $1
		si_WE <= '1'; 
          s_immediate <= x"00000001";
          s_ALU_sc <= '1';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;

          s_rd <= "00010";                   -- #Place 2 in $2
		si_WE <= '1'; 
          s_immediate <= x"00000002";
          s_ALU_sc <= '1';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;

          s_rd <= "00011";                   -- #Place 3 in $3
		si_WE <= '1'; 
          s_immediate <= x"00000003";
          s_ALU_sc <= '1';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;

          s_rd <= "00100";                   -- #Place 4 in $4
		si_WE <= '1'; 
          s_immediate <= x"00000004";
          s_ALU_sc <= '1';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;

          s_rd <= "00101";                   -- #Place 5 in $5
		si_WE <= '1'; 
          s_immediate <= x"00000005";
          s_ALU_sc <= '1';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;

          s_rd <= "00110";                   -- #Place 6 in $6
		si_WE <= '1'; 
          s_immediate <= x"00000006";
          s_ALU_sc <= '1';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;

          s_rd <= "00111";                   -- #Place 7 in $7
		si_WE <= '1'; 
          s_immediate <= x"00000007";
          s_ALU_sc <= '1';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;

          s_rd <= "01000";                   -- #Place 8 in $8
		si_WE <= '1'; 
          s_immediate <= x"00000008";
          s_ALU_sc <= '1';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;

          s_rd <= "01001";                   -- #Place 9 in $9
		si_WE <= '1'; 
          s_immediate <= x"00000009";
          s_ALU_sc <= '1';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;

          s_rd <= "01010";                   -- #Place 10 in $10
		si_WE <= '1'; 
          s_immediate <= x"0000000A";
          s_ALU_sc <= '1';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;

          s_rs <= "00001";
          s_rt <= "00010";
          s_rd <= "01011";                   -- #Place $1 + $2 in $11; Expected Value: 3
		si_WE <= '1'; 
          s_immediate <= x"0000000A";
          s_ALU_sc <= '0';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;

          s_rs <= "01011";
          s_rt <= "00011";
          s_rd <= "01100";                   -- #Place $11 - $3 in $12; Expected Value: 0
		si_WE <= '1'; 
          s_immediate <= x"0000000A";
          s_ALU_sc <= '0';
          sn_add_sub <= '1';
		
		wait for gCLK_HPER*2;

	  s_rs <= "01100";
          s_rt <= "00100";
          s_rd <= "01101";                   -- #Place $12 + $4 in $13; Expected Value: 4
		si_WE <= '1'; 
          s_immediate <= x"0000000A";
          s_ALU_sc <= '0';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;
	
	  s_rs <= "01101";
          s_rt <= "00101";
          s_rd <= "01110";                   -- #Place $13 - $5 in $14; Expected Value: -1
		si_WE <= '1'; 
          s_immediate <= x"0000000A";
          s_ALU_sc <= '0';
          sn_add_sub <= '1';
		
		wait for gCLK_HPER*2;

	  s_rs <= "01110";
          s_rt <= "00110";
          s_rd <= "01111";                   -- #Place $14 + $6 in $15; Expected Value: 5
		si_WE <= '1'; 
          s_immediate <= x"0000000A";
          s_ALU_sc <= '0';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;

	s_rs <= "01111";
          s_rt <= "00111";
          s_rd <= "10000";                   -- #Place $15 - $7 in $16; Expected Value: -2
		si_WE <= '1'; 
          s_immediate <= x"0000000A";
          s_ALU_sc <= '0';
          sn_add_sub <= '1';
		
		wait for gCLK_HPER*2;

	s_rs <= "10000";
          s_rt <= "01000";
          s_rd <= "10001";                   -- #Place $16 + $8 in $17; Expected Value: 6
		si_WE <= '1'; 
          s_immediate <= x"0000000A";
          s_ALU_sc <= '0';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;

	s_rs <= "10001";
          s_rt <= "01001";
          s_rd <= "10010";                   -- #Place $17 - $9 in $18; Expected Value: -3
		si_WE <= '1'; 
          s_immediate <= x"0000000A";
          s_ALU_sc <= '0';
          sn_add_sub <= '1';
		
		wait for gCLK_HPER*2;

	s_rs <= "10010";
          s_rt <= "01010";
          s_rd <= "10011";                   -- #Place $18 + $10 in $19; Expected Value: 7
		si_WE <= '1'; 
          s_immediate <= x"0000000A";
          s_ALU_sc <= '0';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;

          s_rs <= "00000";
          s_rd <= "10100";                   -- #Place -35 + 0 in $20; Expected Value: -35
		si_WE <= '1'; 
          s_immediate <= x"FFFFFFDD";        --35 in twos complement
          s_ALU_sc <= '1';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;

	s_rs <= "10011";
          s_rt <= "10100";
          s_rd <= "10101";                   -- #Place $19 + $20 in $21; Expected Value: -28
		si_WE <= '1'; 
          s_immediate <= x"0000000A";
          s_ALU_sc <= '0';
          sn_add_sub <= '0';
		
		wait for gCLK_HPER*2;

	end process;
end tb_my_first_datapath_arch;