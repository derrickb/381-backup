--Derrick Brandt
--CPRE 381 Lab 2
--Professor Berk

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;  -- For logic types I/O
library std;
use std.env.all;                -- For hierarchical/external signals
use std.textio.all;             -- For basic I/O


--entity
entity tb_Nbit_register is
	generic(N : integer := 32; gCLK_HPER   : time := 50 ns);
end tb_Nbit_register;

--architecture

architecture tb_Nbit_register_arch of tb_Nbit_register is 

--VARIABLE
constant cCLK_PER  : time := gCLK_HPER * 2;

--components
component Nbit_register is 
generic(N : integer := 32);
	port(i_CLK	: in std_logic;
	     i_RST	: in std_logic;
	     i_WE	: in std_logic;
	     i_D	: in std_logic_vector(N-1 downto 0);
	     o_Q	: out std_logic_vector(N-1 downto 0));
end component;

--intermediate signals
signal si_D, so_Q		: std_logic_vector(N-1 downto 0);
signal CLK, reset, si_WE : std_logic := '0';
--begin

begin
	DUT0: Nbit_register port map(i_CLK => CLK,
				     i_RST => reset,
				     i_WE  => si_WE,
				     i_D   => si_D,
				     o_Q   => so_Q);
	 P_CLK: process
  		begin
    		CLK <= '1';          -- clock starts at 1
    		wait for gCLK_HPER; 	-- after half a cycle
    		CLK <= '0';          -- clock becomes a 0 (negative edge)
    		wait for gCLK_HPER;     -- after half a cycle, process begins evaluation again
  	end process;

	P_RST: process
  	 begin
  		reset <= '0';   
    	 wait for gCLK_HPER/2;
		reset <= '1';
   	 wait for gCLK_HPER*2;
		reset <= '0';
		wait;
  	 end process;  
	
	P_TESTCASES: process
	begin
		wait for gCLK_HPER*2; -- for waveform clarity, I prefer not to change inputs on clk edges

		si_D <= x"0000FFFF";
		si_WE <= '1'; 
		
		wait for gCLK_HPER*2;
		
		si_D <= x"00000007";
		si_WE <= '1'; 

		wait for gCLK_HPER*2;

		si_D <= x"01010101"; 
		si_WE <= '1'; 

		wait for gCLK_HPER*2;

	end process;

end tb_Nbit_register_arch;