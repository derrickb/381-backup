--Derrick Brandt
--CPRE 381 Lab 2 Section 6
--Professor Berk

-- The Big Boy; The Lochness Monster; The Register File Incarnate.
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std;
library work;
use work.MUX32.all;

--entity
entity registerFile is 
generic(N : integer := 32);
port (read_reg_1, read_reg_2	: in std_logic_vector(4 downto 0);
      i_WE			: in std_logic;
      CLK			: in std_logic;
      RST			: in std_logic;
      i_reg_data		: in std_logic_vector(31 downto 0);
      i_writeto_reg		: in std_logic_vector(4 downto 0);
      o_read_data_1, o_read_data_2 : out std_logic_vector(31 downto 0));
end registerFile;

--architecture
architecture regFile_Arch of registerFile is

--components
--for choosing which register to write
component decoder is 
generic(N   : integer   := 32);
port(D_IN   : in std_logic_vector(4 downto 0);
     i_WE   : in std_logic;
     S_OUT  : out std_logic_vector(N-1 downto 0));
end component;

--and gate for selecting the correct register to write with decoder
component andg2 is
port(i_A          : in std_logic;
     i_B          : in std_logic;
     o_F          : out std_logic);
end component;

--for choosing which registers to read
component MUX_32_One is
generic(N : integer := 32);
port (i_S	: in std_logic_vector(4 downto 0);
      i_D0	: in muxarray;
      S_0	: out std_logic_vector(31 downto 0));
end component;

--need 32, 32 bit registers
component Nbit_register is 
generic(N : integer := 32);
port(i_CLK	: in std_logic;
     i_RST	: in std_logic;

     i_WE	: in std_logic;
     i_D	: in std_logic_vector(N-1 downto 0); 
     o_Q	: out std_logic_vector(N-1 downto 0));
end component;


--signals
signal s_decoder_out, s_andg_decoder	: std_logic_vector(N-1 downto 0);
signal s_reg_out			: muxarray;

--begin
begin 
	TTRegList: for i in 0 to N-1 generate
    	RegiFile: Nbit_register port map(i_CLK => CLK,		--what to do here?
					 i_RST => RST,		--what to do here?
					 i_WE  => s_andg_decoder(i) ,
					 i_D   => i_reg_data ,
					 o_Q   => s_reg_out(i));
 
		Nbit_and:  andg2 port map(i_A => i_WE,
				 i_B => s_decoder_out(i),
				 o_F => s_andg_decoder(i));
		  
  	end generate TTRegList;

	write_reg_decoder: decoder port map(D_IN => i_writeto_reg,
				   	    i_WE => i_WE,
				   	    S_OUT => s_decoder_out);

	mux_read_reg1: MUX_32_One port map(i_S  => read_reg_1,
					   i_D0 => s_reg_out, 		--is this legal?
					   S_0  => o_read_data_1);

	mux_read_reg2: MUX_32_One port map(i_S  => read_reg_2,
					   i_D0 => s_reg_out,		--is this legal?
					   S_0  => o_read_data_2);

end regFile_Arch;