--Derrick Brandt
--CPRE 381 Lab 2 Section 6
--Professor Berk

-- register ARRAY
--compile before everything else
library IEEE;
use IEEE.std_logic_1164.all;

package register32Array is
type regArray is array (31 downto 0) of std_logic_vector(31 downto 0);
end package register32Array;