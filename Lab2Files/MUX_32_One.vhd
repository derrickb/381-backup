--Derrick Brandt
--CPRE 381 Lab 2 Section 6
--Professor Berk

--32 to 1 MULTIPLEXOR

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std;
library work;
use work.MUX32.all;

--entity
entity MUX_32_One is
generic(N : integer := 32);
port (i_S	: in std_logic_vector(4 downto 0);
      i_D0	: in muxarray;
      S_0	: out std_logic_vector(31 downto 0));
end MUX_32_One;

--architecture
architecture MUX_32_One_Arch of MUX_32_One is 
--components
--intermediate signals
signal smux_array	: muxarray;

--begin
begin
	with (i_S) select
	S_0 <=  	i_D0(0) when "00000",
			i_D0(1) when "00001",
			i_D0(2) when "00010",
			i_D0(3) when "00011",--3
		
			i_D0(4) when "00100", --4
			i_D0(5) when "00101",
			i_D0(6) when "00110",
			i_D0(7) when "00111", --7
		
			i_D0(8) when "01000", --8
			i_D0(9) when "01001",
			i_D0(10) when "01010",
			i_D0(11) when "01011", --11

			i_D0(12) when "01100", --12
			i_D0(13) when "01101",
			i_D0(14) when "01110",
			i_D0(15) when "01111", --15
	---------------------------------------
			i_D0(16) when "10000", --16
			i_D0(17) when "10001",
			i_D0(18) when "10010",
			i_D0(19) when "10011", --19
		
			i_D0(20) when "10100", --20
			i_D0(21) when "10101",
			i_D0(22) when "10110",
			i_D0(23) when "10111", --23
		
			i_D0(24) when "11000", --24
			i_D0(25) when "11001",
			i_D0(26) when "11010",
			i_D0(27) when "11011", --27

			i_D0(28) when "11100", --28
			i_D0(29) when "11101",
			i_D0(30) when "11110",
			i_D0(31) when "11111",
			x"00000000" when others; --31






end MUX_32_One_Arch;