##Derrick Brandt
##CPRE 381 Lab 2 Section 6
##10/02/21
##Professor Berk

#Test Compiling Things
vcom -reportprogress 300 -work work {/home/derrickb/derrick_b/CPRE 381/Lab2/Lab2/MUX32Package.vhd} -2008
vcom -reportprogress 300 -work work {/home/derrickb/derrick_b/CPRE 381/Lab2/Lab2/*.vhd} -2008
vcom -reportprogress 300 -work work {/home/derrickb/derrick_b/CPRE 381/Lab2/Lab2/Lab1Files/*.vhd} -2008

#IT WORKS! HA HA

#Test starting simulation
vsim work.tb_my_first_datapath

#log command for adding future waves
log -r *

#Test adding waves
add wave -position insertpoint sim:/tb_my_first_datapath/DUTDATPATH/*

#test adding more waves
add wave -position insertpoint  \
sim:/tb_my_first_datapath/sCLK

add wave -position insertpoint  \
sim:/tb_my_first_datapath/reset

add wave -position insertpoint  \
sim:/tb_my_first_datapath/DUTDATPATH/regiFile/s_reg_out

run 2400 ns