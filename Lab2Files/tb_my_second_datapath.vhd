--Derrick Brandt
--CPRE 381 Lab 2 Section 6
--Professor Berk

--Test Bench for My Second Datapath
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;  -- For logic types I/O
library std;
use std.env.all;                -- For hierarchical/external signals
use std.textio.all;             -- For basic I/O

--entity
entity tb_my_second_datapath is 
--generic
generic(N : integer := 32; gCLK_HPER   : time := 50 ns);
end tb_my_second_datapath;

--architecture
architecture tb_my_second_datapath_arch of tb_my_second_datapath is 
--VARIABLE
constant cCLK_PER  : time := gCLK_HPER * 2;

--components
component my_second_datapath is 
     generic(N : integer := 32);
     port(i_read_rs, i_read_rt, i_write_rd	: in std_logic_vector(4 downto 0);
          i_ALUsc, i_ext_sign_control, i_nAdd_Sub, i_CLK, i_write_en, i_reset, mem_to_reg	, mem_write: in std_logic; --CONTROL SIGNALS
          i_bit_imm_16	: in std_logic_vector(15 downto 0));
end component;

--signals
     signal s_rs, s_rt, s_rd	         : std_logic_vector(4 downto 0);
     --control signals
     signal sCLK, reset, si_WE        : std_logic := '0';
     signal s_ALU_sc, sn_add_sub	     : std_logic;
     signal s_immediate               : std_logic_vector(15 downto 0);
     --mem  control signals
     signal s_mem_to_reg, s_mem_write : std_logic := '0';
     --extender control signal
     signal s_ext_control            : std_logic := '0';
--begin

begin
        DUTSECONDPATH: my_second_datapath port map(i_read_rs    => s_rs,
                                                   i_read_rt    => s_rt,
                                                   i_write_rd   => s_rd,
                                                   i_ALUsc        => s_ALU_sc,
                                                   i_nAdd_Sub   => sn_add_sub,
                                                   i_write_en   => si_WE,
                                                   i_CLK        => sCLK,
                                                   i_reset      => reset,
                                                   i_bit_imm_16 => s_immediate,
                                                   i_ext_sign_control => s_ext_control,
                                                   mem_to_reg => s_mem_to_reg,
                                                   mem_write => s_mem_write);

        P_CLK: process
  		begin
    		sCLK <= '1';          -- clock starts at 1
    		wait for gCLK_HPER; 	-- after half a cycle
    		sCLK <= '0';          -- clock becomes a 0 (negative edge)
    		wait for gCLK_HPER;     -- after half a cycle, process begins evaluation again
  	    end process;

        P_RST: process
  	    begin
            reset <= '0';   
            wait for gCLK_HPER/2;
            reset <= '1';
            wait for gCLK_HPER*2;
            reset <= '0';
            wait;
  	    end process;

       
        P_TESTCASES: process
	     begin
          -- reset <= '1';
          -- wait for gCLK_HPER*2;
          -- reset <= '0';

	 wait for gCLK_HPER*1; -- for waveform clarity, I prefer not to change inputs on clk edges

         --#TODO TODO TODO TODO TODO
	     s_rd <= "11001";                   -- #Load $A into 25; addi
          s_rs <= "00000";
          si_WE <= '1'; 
          s_immediate <= x"0000";
	     s_ext_control <= '0';
          s_ALU_sc <= '1';
          sn_add_sub <= '0';

          --mem control signals
          s_mem_to_reg <= '1';
          s_mem_write  <= '0';
		wait for gCLK_HPER*2;
          ----------------------------------------------------------------------
          s_rd <= "11010";                   -- #Load $B into 26; addi
          s_rs <= "00000";
          si_WE <= '1'; 
          s_immediate <= x"0100";
	     s_ext_control <= '0';
          s_ALU_sc <= '1';
          sn_add_sub <= '0';

          --mem control signals
          s_mem_to_reg <= '1';
          s_mem_write  <= '0';
		wait for gCLK_HPER*2;
          ------------------------------------------------------------------------
          s_rd <= "00001";                   -- #Load A[0] into $1
          s_rs <= "11001";
          si_WE <= '1'; 
          s_immediate <= x"0000";
	     s_ext_control <= '0';
          s_ALU_sc <= '1';
          sn_add_sub <= '0';

          --mem control signals
          s_mem_to_reg <= '0';
          s_mem_write  <= '0';
		wait for gCLK_HPER*2;

           ------------------------------------------------------------------------
           s_rd <= "00010";                   -- #Load A[1] into $2; lw
           s_rs <= "11001";
           si_WE <= '1'; 
           s_immediate <= x"0001";
           s_ext_control <= '0';
           s_ALU_sc <= '1';
           sn_add_sub <= '0';
 
           --mem control signals
           s_mem_to_reg <= '0';
           s_mem_write  <= '0';
           wait for gCLK_HPER*2;

           ------------------------------------------------------------------------
           s_rd <= "00011";                   -- $3 = $1 + $2; add
           s_rs <= "00001";
           s_rt <= "00010";
           si_WE <= '1'; 
           s_immediate <= x"0001";
           s_ext_control <= '0';
           s_ALU_sc <= '0';
           sn_add_sub <= '0';
 
           --mem control signals
           s_mem_to_reg <= '1';
           s_mem_write  <= '0';
           wait for gCLK_HPER*2;
          ------------------------------------------------------------------------
                                             --#Store $1 into B[0]; sw
           s_rs <= "11010";
           s_rt <= "00001";
           si_WE <= '0'; 
           s_immediate <= x"0000";
           s_ext_control <= '0';
           s_ALU_sc <= '1';
           sn_add_sub <= '0';
 
           --mem control signals
           s_mem_to_reg <= '0';
           s_mem_write  <= '1';
           wait for gCLK_HPER*2;

           ------------------------------------------------------------------------
                                        --#Load A[2] into $2; lw
           s_rd <= "00010"; --2  
           s_rs <= "11001"; --25       --add 2 to $25 and load val into $2
           si_WE <= '1'; 
           s_immediate <= x"0002";
           s_ext_control <= '0';
           s_ALU_sc <= '1';
           sn_add_sub <= '0';
 
           --mem control signals
           s_mem_to_reg <= '0';
           s_mem_write  <= '0';
           wait for gCLK_HPER*2;

          ------------------------------------------------------------------------
          s_rd <= "00001";                   -- $1 = $1 + $2; add
          s_rs <= "00001";
          s_rt <= "00010";
          si_WE <= '1'; 
          s_immediate <= x"0001";
          s_ext_control <= '0';
          s_ALU_sc <= '0';
          sn_add_sub <= '0';

          --mem control signals
          s_mem_to_reg <= '1';
          s_mem_write  <= '0';
          wait for gCLK_HPER*2;

          ------------------------------------------------------------------------
                                             --#Store $1 into B[1]; sw
           s_rs <= "11010";
           s_rt <= "00001";
           si_WE <= '0'; 
           s_immediate <= x"0001";
           s_ext_control <= '0';
           s_ALU_sc <= '1';
           sn_add_sub <= '0';
 
           --mem control signals
           s_mem_to_reg <= '0';
           s_mem_write  <= '1';
           wait for gCLK_HPER*2;
     	 ------------------------------------------------------------------------
                                        --#Load A[3] into $2; lw
           s_rd <= "00010"; --2  
           s_rs <= "11001"; --25       --add 3 to $25 and load val into $2
           si_WE <= '1'; 
           s_immediate <= x"0003";
           s_ext_control <= '0';
           s_ALU_sc <= '1';
           sn_add_sub <= '0';
 
           --mem control signals
           s_mem_to_reg <= '0';
           s_mem_write  <= '0';
           wait for gCLK_HPER*2;
   	------------------------------------------------------------------------
          s_rd <= "00001";                   -- $1 = $1 + $2; add
          s_rs <= "00001";
          s_rt <= "00010";
          si_WE <= '1'; 
          s_immediate <= x"0001";
          s_ext_control <= '0';
          s_ALU_sc <= '0';
          sn_add_sub <= '0';

          --mem control signals
          s_mem_to_reg <= '1';
          s_mem_write  <= '0';
          wait for gCLK_HPER*2;
 	------------------------------------------------------------------------
                                             --#Store $1 into B[2]; sw
           s_rs <= "11010";
           s_rt <= "00001";
           si_WE <= '0'; 
           s_immediate <= x"0002";
           s_ext_control <= '0';
           s_ALU_sc <= '1';
           sn_add_sub <= '0';
 
           --mem control signals
           s_mem_to_reg <= '0';
           s_mem_write  <= '1';
           wait for gCLK_HPER*2;
	 ------------------------------------------------------------------------
                                        --#Load A[4] into $2; lw
           s_rd <= "00010"; --2  
           s_rs <= "11001"; --25       --add 4 to $25 and load val into $2
           si_WE <= '1'; 
           s_immediate <= x"0004";
           s_ext_control <= '0';
           s_ALU_sc <= '1';
           sn_add_sub <= '0';
 
           --mem control signals
           s_mem_to_reg <= '0';
           s_mem_write  <= '0';
           wait for gCLK_HPER*2;
	------------------------------------------------------------------------
          s_rd <= "00001";                   -- $1 = $1 + $2; add
          s_rs <= "00001";
          s_rt <= "00010";
          si_WE <= '1'; 
          s_immediate <= x"0001";
          s_ext_control <= '0';
          s_ALU_sc <= '0';
          sn_add_sub <= '0';

          --mem control signals
          s_mem_to_reg <= '1';
          s_mem_write  <= '0';
          wait for gCLK_HPER*2;
	 ------------------------------------------------------------------------
                                             --#Store $1 into B[3]; sw
           s_rs <= "11010";
           s_rt <= "00001";
           si_WE <= '0'; 
           s_immediate <= x"0003";
           s_ext_control <= '0';
           s_ALU_sc <= '1';
           sn_add_sub <= '0';
 
           --mem control signals
           s_mem_to_reg <= '0';
           s_mem_write  <= '1';
           wait for gCLK_HPER*2;

	 ------------------------------------------------------------------------
                                        --#Load A[5] into $2; lw
           s_rd <= "00010"; --2  
           s_rs <= "11001"; --25       --add 5 to $25 and load val into $2
           si_WE <= '1'; 
           s_immediate <= x"0005";
           s_ext_control <= '0';
           s_ALU_sc <= '1';
           sn_add_sub <= '0';
 
           --mem control signals
           s_mem_to_reg <= '0';
           s_mem_write  <= '0';
           wait for gCLK_HPER*2;
	------------------------------------------------------------------------
          s_rd <= "00001";                   -- $1 = $1 + $2; add
          s_rs <= "00001";
          s_rt <= "00010";
          si_WE <= '1'; 
          s_immediate <= x"0001";
          s_ext_control <= '0';
          s_ALU_sc <= '0';
          sn_add_sub <= '0';

          --mem control signals
          s_mem_to_reg <= '1';
          s_mem_write  <= '0';
          wait for gCLK_HPER*2;
	------------------------------------------------------------------------
                                             --#Store $1 into B[4]; sw
           s_rs <= "11010";
           s_rt <= "00001";
           si_WE <= '0'; 
           s_immediate <= x"0004";
           s_ext_control <= '0';
           s_ALU_sc <= '1';
           sn_add_sub <= '0';
 
           --mem control signals
           s_mem_to_reg <= '0';
           s_mem_write  <= '1';
           wait for gCLK_HPER*2;
	------------------------------------------------------------------------
                                        --#Load A[6] into $2; lw
           s_rd <= "00010"; --2  
           s_rs <= "11001"; --25       --add 6 to $25 and load val into $2
           si_WE <= '1'; 
           s_immediate <= x"0006";
           s_ext_control <= '0';
           s_ALU_sc <= '1';
           sn_add_sub <= '0';
 
           --mem control signals
           s_mem_to_reg <= '0';
           s_mem_write  <= '0';
           wait for gCLK_HPER*2;
	------------------------------------------------------------------------
          s_rd <= "00001";                   -- $1 = $1 + $2; add
          s_rs <= "00001";
          s_rt <= "00010";
          si_WE <= '1'; 
          s_immediate <= x"0001";
          s_ext_control <= '0';
          s_ALU_sc <= '0';
          sn_add_sub <= '0';

          --mem control signals
          s_mem_to_reg <= '1';
          s_mem_write  <= '0';
          wait for gCLK_HPER*2;
	----------------------------------------------------------------------
          s_rd <= "11011";                   -- #Load $B[256] into 27; addi
          s_rs <= "00000";
          si_WE <= '1'; 
          s_immediate <= x"0200";
	  s_ext_control <= '0';
          s_ALU_sc <= '1';
          sn_add_sub <= '0';

          --mem control signals
          s_mem_to_reg <= '1';
          s_mem_write  <= '0';
	  wait for gCLK_HPER*2;
	------------------------------------------------------------------------
                                             --#Store $1 into B[255]; sw
           s_rs <= "11011";
           s_rt <= "00001";
           si_WE <= '0'; 
           s_immediate <= x"0001";
           s_ext_control <= '0';
           s_ALU_sc <= '1';
           sn_add_sub <= '1';
 
           --mem control signals
           s_mem_to_reg <= '0';
           s_mem_write  <= '1';
           wait for gCLK_HPER*2;




	end process;
        
end tb_my_second_datapath_arch;
