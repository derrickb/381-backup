--Derrick Brandt
--CPRE 381 Lab 2 Section 1
--Professor Berk

--My Second Datapath

library IEEE;
use IEEE.std_logic_1164.all;

--entity
entity my_second_datapath is 
--generic
generic(N : integer := 32);
port(i_read_rs, i_read_rt, i_write_rd	: in std_logic_vector(4 downto 0);
     i_ALUsc, i_ext_sign_control, i_nAdd_Sub, i_CLK, i_write_en, i_reset, mem_to_reg	, mem_write: in std_logic; --CONTROL SIGNALS
     i_bit_imm_16	: in std_logic_vector(15 downto 0));
end my_second_datapath;

--architecture
architecture my_second_datapath_arch of my_second_datapath is 
--COMPONENTS-------------------------------------------------

---> first datapath components:

--adder subtractor
component adder_subtractor is 
generic(N : integer := 32);
port(A, B		     : in std_logic_vector(N-1 downto 0);
     nAdd_Sub		: in std_logic;
     Add_Sub_out	: out std_logic_vector(N-1 downto 0));
end component;

--registerFile
component registerFile is 
generic(N : integer := 32);
port (read_reg_1, read_reg_2	: in std_logic_vector(4 downto 0);
      i_WE			: in std_logic;
      CLK			     : in std_logic;
      RST			     : in std_logic;
      i_reg_data		: in std_logic_vector(31 downto 0);
      i_writeto_reg		: in std_logic_vector(4 downto 0);
      o_read_data_1, o_read_data_2 : out std_logic_vector(31 downto 0));
end component;

--memory
component mem is
	generic 
	(
		DATA_WIDTH : natural := 32;
		ADDR_WIDTH : natural := 10
	);

	port 
	(
		clk		   : in std_logic;
		addr	        : in std_logic_vector((ADDR_WIDTH-1) downto 0);
		data	        : in std_logic_vector((DATA_WIDTH-1) downto 0);
		we		   : in std_logic := '1';
		q		: out std_logic_vector((DATA_WIDTH -1) downto 0)
	);
end component;

--extender
component bit_width_extender is 
generic(N : integer := 32);
port(control	: in std_logic;
     i_A	     : in std_logic_vector(15 downto 0);
     o_F	: out std_logic_vector(N-1 downto 0));
end component;

--mux for memory or register value
component mux2t1_N is
  generic(N : integer := 32); -- Generic of type integer for input/output data width. Default value is 32.
  port(i_S          : in std_logic;
       i_D0         : in std_logic_vector(N-1 downto 0);
       i_D1         : in std_logic_vector(N-1 downto 0);
       o_O          : out std_logic_vector(N-1 downto 0));

end component;

--signals
--for extender
signal s_o_F	 : std_logic_vector(N-1 downto 0);
--THE ACTUALLY IMPORTANT SIGNALS!!!!!!!!!!!!!!!!!!!!!!!!!
signal s_b_mux_out, s_addersubber_out	: std_logic_vector(31 downto 0);
signal s_memtoreg_data			     : std_logic_vector(31 downto 0);
signal s_o_read_data_1, s_o_read_data_2 : std_logic_vector(31 downto 0);
signal s_q_out                          : std_logic_vector(31 downto 0);

--begin
begin 
	regiFile: registerFile port map(read_reg_1    => i_read_rs,
                                        read_reg_2    => i_read_rt,
                                        i_WE          => i_write_en,
                                        CLK           => i_CLK,
                                        RST           => i_reset,
                                        i_reg_data    => s_memtoreg_data, --IMPORTANT
                                        i_writeto_reg => i_write_rd,
                                        o_read_data_1 => s_o_read_data_1,
                                        o_read_data_2 => s_o_read_data_2);
	
	addiSubi: adder_subtractor port map(A         => s_o_read_data_1,
                                         B            => s_b_mux_out,
                                         nAdd_Sub     => i_nAdd_Sub,
                                         Add_Sub_out  => s_addersubber_out);

	muxi_1:	mux2t1_N port map(        i_s         => i_ALUsc,
                                         i_D0         => s_o_read_data_2,
                                         i_D1         => s_o_F,		--takes value from extender
                                         o_O          => s_b_mux_out);

	
	memory: mem port map(clk                      => i_CLK,	
			           addr               => s_addersubber_out(9 downto 0), --#TODO 
			           data               => s_o_read_data_2, --#TODO
			           we                 => mem_write,
			           q                  => s_q_out); --#TODO ?? Goes to another mux?

	extender: bit_width_extender port map(control => i_ext_sign_control,
					      i_A     => i_bit_imm_16,	--#TODO ??
					      o_F     => s_o_F);
	
	muxi_2: mux2t1_N port map(i_S                    => mem_to_reg,		--THE IMPORTANT MUX!!!!!!!!!!!!
			 	           i_D0                   => s_q_out,
				           i_D1                   => s_addersubber_out, --#TODO :: need signals
				           o_O                    => s_memtoreg_data);
	
end my_second_datapath_arch;