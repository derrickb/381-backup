--Derrick Brandt
--CPRE 381 Lab 2 Section 6
--10/02/21
--Professor Berk

--Test Bench for mem

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;  -- For logic types I/O
library std;
use std.env.all;                -- For hierarchical/external signals
use std.textio.all;             -- For basic I/O

--entity
entity tb_dmem is 
generic (
		DATA_WIDTH : natural := 32;
		ADDR_WIDTH : natural := 10;
		gCLK_HPER   : time := 50 ns
	);
end tb_dmem;

--architecture
architecture tb_dmem_arch of tb_dmem is

constant cCLK_PER  : time := gCLK_HPER * 2;

--components
component mem is

	generic 
	(
		DATA_WIDTH : natural := 32;
		ADDR_WIDTH : natural := 10
	);

	port 
	(
		clk		: in std_logic;
		addr	        : in std_logic_vector((ADDR_WIDTH-1) downto 0);
		data	        : in std_logic_vector((DATA_WIDTH-1) downto 0);
		we		: in std_logic := '1';
		q		: out std_logic_vector((DATA_WIDTH -1) downto 0)
	);

end component;
--signals
signal sCLK, sWE 	 : std_logic := '0';
signal s_addr	 	 : std_logic_vector((ADDR_WIDTH-1) downto 0);
signal s_data, s_q_out	 : std_logic_vector((DATA_WIDTH-1) downto 0);
--begin
begin 
	dmem: mem port map(  clk  => sCLK,		--instantiate as dmem
			     addr => s_addr,
			     data => s_data,
			     we   => sWE,
			     q    => s_q_out);

	P_CLK: process
  		begin
    		sCLK <= '1';          -- clock starts at 1
    		wait for gCLK_HPER; 	-- after half a cycle
    		sCLK <= '0';          -- clock becomes a 0 (negative edge)
    		wait for gCLK_HPER;     -- after half a cycle, process begins evaluation again
  	end process;
	
	P_TESTCASES: process
		begin
		
		wait for gCLK_HPER*2;		

		sWE <= '1';			--1st read value
		wait for gCLK_HPER*2;

		sWE <= '1';			--2
		wait for gCLK_HPER*2;

		sWE <= '1';			--3
		wait for gCLK_HPER*2;

		sWE <= '1';			--4
		wait for gCLK_HPER*2;

		sWE <= '1';			--5
		wait for gCLK_HPER*2;

		sWE <= '1';			--6
		wait for gCLK_HPER*2;

		sWE <= '1';			--7
		wait for gCLK_HPER*2;

		sWE <= '1';			--8
		wait for gCLK_HPER*2;

		sWE <= '1';			--9
		wait for gCLK_HPER*2;

		sWE <= '1';			--10
		wait for gCLK_HPER*2;
	end process;

end tb_dmem_arch;