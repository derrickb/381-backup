--Derrick Brandt 
--CPRE 381 Lab 2 Section 6
--Professor Berk

--Test Bench for Bit Extender

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;  -- For logic types I/O
library std;
use std.env.all;                -- For hierarchical/external signals
use std.textio.all;             -- For basic I/O

--entity
entity tb_bit_width_extender is 
--generic
generic(N : integer := 32);

end tb_bit_width_extender;

--architecture
architecture tb_bit_width_extender_arch of tb_bit_width_extender is 

--components
component bit_width_extender is 
generic(N : integer := 32);
port(control	: in std_logic;
     i_A	: in std_logic_vector(15 downto 0);
     o_F	: out std_logic_vector(N-1 downto 0));
end component;

--signals
signal s_control : std_logic;
signal s_i_A	 : std_logic_vector(15 downto 0);
signal s_o_F	 : std_logic_vector(N-1 downto 0);

--begin
begin
	DUTEXT: bit_width_extender port map(control => s_control,
					    i_A     => s_i_A,
					    o_F     => s_o_F);
	

	P_TESTCASES:process
	 
	begin 
	s_control <= '0';
	s_i_A <= x"1234";
	wait for 100 ns;

	s_control <= '1';
	s_i_A <= x"5678";
	wait for 100 ns;
	
	s_control <= '0';
	s_i_A <= x"ABAB";
	wait for 100 ns;

	s_control <= '1';
	s_i_A <= x"FFFF";
	wait;

	end process;

end tb_bit_width_extender_arch;