-- Team G NULL

library IEEE;
use IEEE.std_logic_1164.all;

entity lui is

  port(i_A          : in std_logic_vector(31 downto 0);
       o_O          : out std_logic_vector(31 downto 0));

end lui;

architecture dataflow of lui is

--components

--signals

--begin
begin

  o_O <= i_A sll 16;
  
end dataflow;
